import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const isUserLoggedIn = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`customer/isLogin`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in isUserLoggedIn.', error)
                    )
                    .then(response => dispatch(isUserLoggedInAction(response)));
    }
}

function isUserLoggedInAction(customerApiResponse) {
    let customerLoginStatus = false;
    if(customerApiResponse && customerApiResponse.status == 200) {
        customerLoginStatus = customerApiResponse.data
    }

    return {
        type: ActionConstants.IS_USER_LOGGED_IN,
        data: customerLoginStatus
    }
}