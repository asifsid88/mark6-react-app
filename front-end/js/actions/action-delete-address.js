import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const deleteAddress = (addressId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`customer/deleteAddress?addressId=${addressId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in deleteAddress.', error)
                    )
                    .then(response => dispatch(deleteAddressAction(response)));
    }
}

function deleteAddressAction(addressApiResponse) {
    let address = null;
    if(addressApiResponse && addressApiResponse.status == 200) {
        address = addressApiResponse.data
    }

    return {
        type: ActionConstants.DELETE_ADDRESS,
        data: address
    }
}