import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const saveAddress = (address) => {
    return (dispatch) => {
        const config = { headers: { 'Content-Type': 'application/json;charset=utf-8' } };
        return axios.post(getApiURL(`customer/saveAddress`),
                          address,
                          config)
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in saveAddress.', error)
                    )
                    .then(response => dispatch(saveAddressAction(response)));
    }
}

function saveAddressAction(addressApiResponse) {
    let address = null;
    if(addressApiResponse && addressApiResponse.status == 200) {
        address = addressApiResponse.data
    }

    return {
        type: ActionConstants.SAVE_ADDRESS,
        data: address
    }
}