import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const getWishlist = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`wishlist/home`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getWishlist.', error)
                    )
                    .then(response => dispatch(getWishlistAction(response)));
    }
}

function getWishlistAction(wishlistApiResponse) {
    let wishlist = null;
    if(wishlistApiResponse && wishlistApiResponse.status == 200) {
        wishlist = wishlistApiResponse.data
    }

    return {
        type: ActionConstants.GET_WISHLIST,
        data: wishlist
    }
}