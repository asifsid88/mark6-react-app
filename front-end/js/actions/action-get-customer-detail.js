import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const getCustomerDetail = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`customer/detail`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getCustomerDetail.', error)
                    )
                    .then(response => dispatch(getCustomerDetailAction(response)));
    }
}

function getCustomerDetailAction(customerApiResponse) {
    let customerDetail = null;
    if(customerApiResponse && customerApiResponse.status == 200) {
        customerDetail = customerApiResponse.data;
    }

    return {
        type: ActionConstants.GET_CUSTOMER_DETAIL,
        data: customerDetail
    }
}