import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const selectAddress = (addressId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`checkout/selectAddress?addressId=${addressId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in selectAddress.', error)
                    )
                    .then(response => dispatch(selectAddressAction(response)));
    }
}

function selectAddressAction(addressApiResponse) {
    let address = null;
    if(addressApiResponse && addressApiResponse.status == 200) {
        address = addressApiResponse.data
    }

    return {
        type: ActionConstants.SELECT_ADDRESS,
        data: address
    }
}