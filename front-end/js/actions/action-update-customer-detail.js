import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const updateCustomerDetail = (customer) => {
    return (dispatch) => {
        const config = { headers: { 'Content-Type': 'application/json;charset=utf-8' } };
        return axios.post(getApiURL(`customer/updateDetail`),
                          customer,
                          config)
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in updateCustomerDetail.', error)
                    )
                    .then(response => dispatch(updateCustomerDetailAction(response)));
    }
}

function updateCustomerDetailAction(customerApiResponse) {
    let customerDetail = null;
    if(customerApiResponse && customerApiResponse.status == 200) {
        customerDetail = customerApiResponse.data;
    }

    return {
        type: ActionConstants.UPDATE_CUSTOMER_DETAIL,
        data: customerDetail
    }
}