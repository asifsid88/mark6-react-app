import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const getProductById = (productId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`product/${productId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getProductById: ' + productId, error)
                    )
                    .then(response => dispatch(getProductByIdAction(response)));
    }
}

function getProductByIdAction(productApiResponse) {
    let product = null;
    if(productApiResponse && productApiResponse.status == 200) {
        product = productApiResponse.data
    }

    return {
        type: ActionConstants.GET_PRODUCT_BY_ID,
        data: product
    }
}