import * as ActionConstants from './action-constants';

export function selectSize(M6SIN, size) {
    return {
        type: ActionConstants.SIZE_SELECTION,
        data: M6SIN + "::" + size
    }
}