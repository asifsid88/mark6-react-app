import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const addToWishlist = (productId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`wishlist/add?productId=${productId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in addToWishlist.', error)
                    )
                    .then(response => dispatch(addToWishlistAction(response)));
    }
}

function addToWishlistAction(wishlistApiResponse) {
    let wishlist = null;
    if(wishlistApiResponse && wishlistApiResponse.status == 200) {
        wishlist = wishlistApiResponse.data
    }

    return {
        type: ActionConstants.ADD_TO_WISHLIST,
        data: wishlist
    }
}