import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const addToCart = (pid, qty) => {
    return (dispatch) => {
        const body = {
                        "pid" : pid,
                        "qty" : qty
                     }

        const config = { headers: { 'Content-Type': 'application/json;charset=utf-8' } };
        return axios.post(getApiURL(`cart/add`),
                          body,
                          config)
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in addToCart.', error)
                    )
                    .then(response => dispatch(addToCartAction(response)));
    }
}

function addToCartAction(cartApiResponse) {
    let cart = null;
    if(cartApiResponse && cartApiResponse.status == 200) {
        cart = cartApiResponse.data;
    }

    return {
        type: ActionConstants.ADD_TO_CART,
        data: cart
    }
}