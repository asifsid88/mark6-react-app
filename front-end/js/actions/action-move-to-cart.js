import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const moveToCart = (productId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`wishlist/move?productId=${productId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in moveToCart.', error)
                    )
                    .then(response => dispatch(moveToCartAction(response)));
    }
}

function moveToCartAction(wishlistApiResponse) {
    let wishlist = null;
    if(wishlistApiResponse && wishlistApiResponse.status == 200) {
        wishlist = wishlistApiResponse.data
    }

    return {
        type: ActionConstants.MOVE_TO_CART,
        data: wishlist
    }
}