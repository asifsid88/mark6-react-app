import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const removeCartItem = (cartItemId, cartRefId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`cart/remove?cartItemId=${cartItemId}&cartRefId=${cartRefId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in removeCartItem.', error)
                    )
                    .then(response => dispatch(removeCartItemAction(response)));
    }
}

function removeCartItemAction(cartApiResponse) {
    let cart = null;
    if(cartApiResponse && cartApiResponse.status == 200) {
        cart = cartApiResponse.data;
    }

    return [
        {
            type: ActionConstants.REMOVE_CART_ITEM,
            data: cart
        },
        {
            type: ActionConstants.CHECKOUT_REMOVE_CART_ITEM,
            data: cart
        }
    ]
}