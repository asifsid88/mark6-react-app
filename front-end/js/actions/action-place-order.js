import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const placeOrder = (checkout) => {
    return (dispatch) => {
        const config = { headers: { 'Content-Type': 'application/json;charset=utf-8' } };
        return axios.post(getApiURL(`checkout/placeOrder`),
                          {checkout},
                          config)
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in placeOrder.', error)
                    )
                    .then(response => dispatch(placeOrderAction(response)));
    }
}

function placeOrderAction(checkoutApiResponse) {
    let orderId = null;
    if(checkoutApiResponse && checkoutApiResponse.status == 200) {
        // Success
        orderId = checkoutApiResponse.data;
    } else {
        // Gather error params
    }

    window.location.href = "/order_details?orderId=" + orderId;

    return {
        type: ActionConstants.NOOP,
        data: null
    }
}