import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const getOrders = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`order/all`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getOrders.', error)
                    )
                    .then(response => dispatch(getOrdersAction(response)));
    }
}

function getOrdersAction(orderApiResponse) {
    let orderList = null;
    if(orderApiResponse && orderApiResponse.status == 200) {
        orderList = orderApiResponse.data
    }

    return {
        type: ActionConstants.GET_ORDERS,
        data: orderList
    }
}