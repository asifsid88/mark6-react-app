import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const getAddressList = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`customer/addresses`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getAddressList.', error)
                    )
                    .then(response => dispatch(getAddressListAction(response)));
    }
}

function getAddressListAction(addressApiResponse) {
    let addressList = null;
    if(addressApiResponse && addressApiResponse.status == 200) {
        addressList = addressApiResponse.data
    }

    return {
        type: ActionConstants.GET_ADDRESS_LIST,
        data: addressList
    }
}