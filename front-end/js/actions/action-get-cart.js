import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const getCart = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`cart/home`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in addToCart.', error)
                    )
                    .then(response => dispatch(getCartAction(response)));
    }
}

function getCartAction(cartApiResponse) {
    let cart = null;
    if(cartApiResponse && cartApiResponse.status == 200) {
        cart = cartApiResponse.data;
    }

    return {
        type: ActionConstants.GET_CART,
        data: cart
    }
}