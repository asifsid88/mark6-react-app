import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const removeWishlistItem = (productId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`wishlist/remove?productId=${productId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in removeWishlistItem.', error)
                    )
                    .then(response => dispatch(removeWishlistItemAction(response)));
    }
}

function removeWishlistItemAction(wishlistApiResponse) {
    let wishlist = null;
    if(wishlistApiResponse && wishlistApiResponse.status == 200) {
        wishlist = wishlistApiResponse.data
    }

    return {
        type: ActionConstants.REMOVE_WISHLIST_ITEM,
        data: wishlist
    }
}