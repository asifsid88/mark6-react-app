import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const getCatalog = () => {
    return (dispatch) => {
        return axios.get(getApiURL(`catalog/home?catalogId=browse`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getCatalog.', error)
                    )
                    .then(response => dispatch(getCatalogAction(response)));
    }
}

function getCatalogAction(catalogApiResponse) {
    let catalog = null;
    if(catalogApiResponse && catalogApiResponse.status == 200) {
        catalog = catalogApiResponse.data
    }

    return {
        type: ActionConstants.GET_CATALOG,
        data: catalog
    }
}