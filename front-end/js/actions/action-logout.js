import * as ActionConstants from './action-constants';
import axios from 'axios';
import * as Util from '../utility';

export const logout = (redirectUrl = "/") => {
    return (dispatch) => {
        return axios.get(Util.getApiURL(`account/logout`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred during logout.', error)
                    )
                    .then(response => dispatch(logoutAction(response, redirectUrl)));
    }
}

function logoutAction(logoutApiResponse, redirectUrl) {
    let logoutUser = null;
    if(logoutApiResponse && logoutApiResponse.status == 200) {
        logoutUser = logoutApiResponse.data;

        window.location.href = decodeURIComponent(redirectUrl);
    }

    return {
        type: ActionConstants.LOGOUT,
        data: logoutUser
    }
}