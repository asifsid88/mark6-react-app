import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const setDefaultAddress = (addressId) => {
    return (dispatch) => {
        return axios.get(getApiURL(`customer/setDefaultAddress?addressId=${addressId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in setDefaultAddress.', error)
                    )
                    .then(response => dispatch(setDefaultAddressAction(response)));
    }
}

function setDefaultAddressAction(addressApiResponse) {
    let address = null;
    if(addressApiResponse && addressApiResponse.status == 200) {
        address = addressApiResponse.data
    }

    return {
        type: ActionConstants.SET_DEFAULT_ADDRESS,
        data: address
    }
}