import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const checkoutInit = (pid = "") => {
    return (dispatch) => {
        return axios.get(getApiURL(`checkout/init?productId=${pid}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in checkoutInit.', error)
                    )
                    .then(response => dispatch(checkoutInitAction(response)));
    }
}

function checkoutInitAction(checkoutApiResponse) {
    let checkout = null;
    if(checkoutApiResponse) {
        if(checkoutApiResponse.status == 200) {
            checkout = checkoutApiResponse.data;
        } else {
            checkout.error = checkoutApiResponse.data;
        }
    }

    return {
        type: ActionConstants.CHECKOUT_INIT,
        data: checkout
    }
}