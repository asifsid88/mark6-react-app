import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';
import {isStringNullOrEmpty} from '../utility';

export const getOrderDetails = (orderId) => {
    if(isStringNullOrEmpty(orderId)) {
        window.location.href = "/";
    }

    return (dispatch) => {
        return axios.get(getApiURL(`order/details?orderId=${orderId}`))
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in getOrderDetails.', error)
                    )
                    .then(response => dispatch(getOrderDetailsAction(response)));
    }
}

function getOrderDetailsAction(orderApiResponse) {
    let order = null;
    if(orderApiResponse && orderApiResponse.status == 200) {
        order = orderApiResponse.data
    }

    return {
        type: ActionConstants.GET_ORDER_DETAILS,
        data: order
    }
}