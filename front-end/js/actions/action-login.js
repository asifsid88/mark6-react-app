import * as ActionConstants from './action-constants';
import axios from 'axios';
import * as Util from '../utility';
import * as QueryString from 'query-string';

export const login = (fbResponse, redirectUrl = "") => {
    return (dispatch) => {
        let body = {};
        if(!fbResponse.status) {
            body = {
                        "socialId" : fbResponse.id,
                        "name" : fbResponse.name,
                        "email" : fbResponse.email,
                        "pictureUrl" : fbResponse.picture.data.url
                    }
        }

        const config = { headers: { 'Content-Type': 'application/json;charset=utf-8' } };
        return axios.post(Util.getApiURL(`login`),
                        body,
                        config)
                    .then(
                        response => response.data,
                        error => console.log('Error occurred in login.', error)
                    )
                    .then(response => dispatch(loginAction(response, redirectUrl)));
    }
}

function loginAction(loginApiResponse, redirectUrl) {
    let loggedinUser = null;
    if(loginApiResponse && loginApiResponse.status == 200) {
        loggedinUser = loginApiResponse.data;

        setUserNameCookie(loggedinUser);
        window.location.href = getReturnUrl(redirectUrl);
    }

    return {
        type: ActionConstants.LOGIN,
        data: loggedinUser
    }
}

function setUserNameCookie(loggedinUser) {
    const fullName = loggedinUser.name.split(' ');
    const firstName = Util.isStringNullOrEmpty(fullName[0]) ? 'User' : fullName[0];
    Util.cookies.setItem('m6', Util.base64Encode(firstName), 24*10, '/');
}

function getReturnUrl(redirectUrl) {
    if(!Util.isStringNullOrEmpty(redirectUrl)) {
        return decodeURIComponent(redirectUrl);
    }

    return getReturnUrlFromQueryString();
}

function getReturnUrlFromQueryString() {
    const queryParams = QueryString.parse(window.location.search);

    let returnUrl = "/";
    if(!Util.isStringNullOrEmpty(queryParams.ret)) {
        returnUrl = decodeURIComponent(queryParams.ret);
    }

    return returnUrl;
}