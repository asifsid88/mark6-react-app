import * as ActionConstants from './action-constants';
import axios from 'axios';
import {getApiURL} from '../utility';

export const updateQuantity = (cartItemId, quantity, cartRefId) => {
    if(quantity <1 || quantity > 10) {
        // This case will never be genuinely true
        return {
            type: ActionConstants.NOOP,
            data: null
        }
    } else {
        return (dispatch) => {
            return axios.get(getApiURL(`cart/updateqty?cartItemId=${cartItemId}&quantity=${quantity}&cartRefId=${cartRefId}`))
                        .then(
                            response => response.data,
                            error => console.log('Error occurred in updateQuantity.', error)
                        )
                        .then(response => dispatch(updateQuantityAction(response)));
        }
    }
}

function updateQuantityAction(cartApiResponse) {
    let cart = null;
    if(cartApiResponse && cartApiResponse.status == 200) {
        cart = cartApiResponse.data;
    }

    return [
        {
            type: ActionConstants.UPDATE_QUANTITY,
            data: cart
        },
        {
            type: ActionConstants.CHECKOUT_UPDATE_QUANTITY,
            data: cart
        }
    ]
}