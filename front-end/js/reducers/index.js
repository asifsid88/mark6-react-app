import {combineReducers} from 'redux';
import CatalogReducer from './action-reducers/reducers-catalog';
import ProductReducer from './action-reducers/reducers-product';
import UserLoggedInReducer from './action-reducers/reducers-user-logged-in';
import LoggedinUserReducer from './action-reducers/reducers-login';
import LogoutUserReducer from './action-reducers/reducers-logout';
import CartReducer from './action-reducers/reducers-cart';
import SizeSelectionReducer from './action-reducers/reducers-selectSize';
import CustomerDetailReducer from './action-reducers/reducers-customer-detail';
import CheckoutReducer from './action-reducers/reducers-checkout';
import AddressListReducer from './action-reducers/reducers-address-list';
import OrderReducer from './action-reducers/reducers-order';
import OrderListReducer from './action-reducers/reducers-order-list';
import WishlistReducer from './action-reducers/reducers-wishlist';

const allReducers = combineReducers({
    catalog: CatalogReducer,
    product: ProductReducer,
    userLoggedIn: UserLoggedInReducer,
    loggedinUser: LoggedinUserReducer,
    logoutUser: LogoutUserReducer,
    cart: CartReducer,
    m6sin: SizeSelectionReducer,
    customer: CustomerDetailReducer,
    checkout: CheckoutReducer,
    addressList: AddressListReducer,
    order: OrderReducer,
    orderList: OrderListReducer,
    wishlist: WishlistReducer
})

export default allReducers;
