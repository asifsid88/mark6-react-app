import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.NOOP:
                return state;
        default:
                return state;
    }
}
