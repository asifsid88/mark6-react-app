import * as ActionConstants from '../../actions/action-constants';
import {deepClone} from '../../utility';

export default function(state=null, action) {
    let updatedState = {};
    switch(action.type) {
        case ActionConstants.SAVE_ADDRESS:
                if(state == null) return state;

                updatedState = deepClone(state);
                updatedState.deliveryAddress = action.data;
                return updatedState;

        case ActionConstants.SELECT_ADDRESS:
                if(state == null) return state;

                updatedState = deepClone(state);
                updatedState.deliveryAddress = action.data;
                return updatedState;

        case ActionConstants.CHECKOUT_UPDATE_QUANTITY:
                if(state == null) return state;

                updatedState = deepClone(state);
                updatedState.orderSummary = action.data;
                return updatedState;

        case ActionConstants.CHECKOUT_REMOVE_CART_ITEM:
                if(state == null) return state;

                updatedState = deepClone(state);
                updatedState.orderSummary = action.data;
                return updatedState;

        case ActionConstants.CHECKOUT_INIT:
                return action.data;

        default:
                return state;
    }
}