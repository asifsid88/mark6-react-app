import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.GET_WISHLIST:
                return action.data;
        case ActionConstants.ADD_TO_WISHLIST:
                return action.data;
        case ActionConstants.REMOVE_WISHLIST_ITEM:
                return action.data;
        case ActionConstants.MOVE_TO_CART:
                return action.data;
        default:
                return state;
    }
}
