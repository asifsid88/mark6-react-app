import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.LOGOUT:
                return false;
        case ActionConstants.LOGIN:
                return true;
        case ActionConstants.IS_USER_LOGGED_IN:
                return action.data;
        default:
                return state;
    }
}
