import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.SAVE_ADDRESS:
                if(state == null) return state;

                const updatedAddress = [];
                state.forEach((address) => {
                    if(address.id != action.data.id) {
                        updatedAddress.push(address);
                    }
                })
                updatedAddress.push(action.data);
                return updatedAddress;

        case ActionConstants.GET_ADDRESS_LIST:
                return action.data;
        default:
                return state;
    }
}
