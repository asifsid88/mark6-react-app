import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.ADD_TO_CART:
                return action.data;
        case ActionConstants.GET_CART:
                return action.data;
        case ActionConstants.REMOVE_CART_ITEM:
                return action.data;
        case ActionConstants.UPDATE_QUANTITY:
                return action.data;
        default:
                return state;
    }
}
