import * as ActionConstants from '../../actions/action-constants';

export default function(state=null, action) {
    switch(action.type) {
        case ActionConstants.GET_PRODUCT_BY_ID:
                return action.data;
        default:
                return state;
    }
}
