import * as ActionConstants from '../../actions/action-constants';
import {deepClone} from '../../utility';

export default function(state=null, action) {
    let updatedState = {};
    let updatedAddress = [];
    let productIdList = [];
    switch(action.type) {
        case ActionConstants.REMOVE_WISHLIST_ITEM:
                if(state == null) return state;

                updatedState = deepClone(state);
                action.data.cartItems.forEach((wishlist) => {
                    productIdList.push(wishlist.productId);
                })
                updatedState.wishlist = productIdList;

                return updatedState;

        case ActionConstants.ADD_TO_WISHLIST:
                if(state == null) return state;

                updatedState = deepClone(state);
                action.data.cartItems.forEach((wishlist) => {
                    productIdList.push(wishlist.productId);
                })
                updatedState.wishlist = productIdList;

                return updatedState;

        case ActionConstants.SET_DEFAULT_ADDRESS:
                if(state == null) return state;

                const addressIdList = [];
                action.data.forEach((address) => {
                    addressIdList.push(address.id);
                })

                state.addressList.forEach((address) => {
                    if(!addressIdList.includes(address.id)) {
                        updatedAddress.push(address);
                    }
                })

                updatedState = deepClone(state);
                updatedState.addressList = updatedAddress.concat(action.data);
                return updatedState;

        case ActionConstants.DELETE_ADDRESS:
                if(state == null) return state;

                updatedState = deepClone(state);
                state.addressList.forEach((address) => {
                    if(address.id != action.data.id) {
                        updatedAddress.push(address);
                    }
                })
                updatedState.addressList = updatedAddress;
                return updatedState;

        case ActionConstants.SAVE_ADDRESS:
                if(state == null) return state;

                updatedState = deepClone(state);
                state.addressList.forEach((address) => {
                    if(address.id != action.data.id) {
                        updatedAddress.push(address);
                    }
                })
                updatedAddress.push(action.data);
                updatedState.addressList = updatedAddress;
                return updatedState;

        case ActionConstants.UPDATE_CUSTOMER_DETAIL:
                return action.data;
        case ActionConstants.GET_CUSTOMER_DETAIL:
                return action.data;
        default:
                return state;
    }
}
