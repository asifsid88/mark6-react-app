import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {createProductPageUrl} from '../utility';
import {bindActionCreators} from 'redux';

class WishlistItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const productUrl = createProductPageUrl(this.props.cartItem.productId,
                                                this.props.cartItem.styleCode,
                                                this.props.cartItem.title);
        return (
            <div className="cart-item-container">
                <div className="cart-product-panel">
                    <div className="cart-product-image">
                        <Link to={productUrl}>
                            <img className="pure-img" src={this.props.cartItem.imageUrl} />
                        </Link>
                    </div>
                    <div className="cart-product-details">
                        <div className="cart-product-title">
                            <Link to={productUrl} className="cart-product-title-link">
                                {this.props.cartItem.title}
                            </Link>
                        </div>
                        <div className="cart-product-color-size">{this.props.cartItem.color}, {this.props.cartItem.size}</div>
                        <div className="cart-product-style-code">{this.props.cartItem.styleCode}</div>
                        <div className="cart-product-price">
                            <div className="cart-product-price-sp">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.cartItem.price.sellingPrice}</div>
                            <div className="cart-product-price-mrp">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.cartItem.price.mrp}</div>
                            <div className="cart-product-price-offer">{this.props.cartItem.price.offer}% Off</div>
                        </div>
                    </div>
                    <div className="cart-product-sla" onClick={() => this.props.removeWishlistItem(this.props.cartItem.productId)}>
                        <div className="float-right">
                            <span className="">
                                <img src="/static/img/bin.svg" className="cursor-pointer" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ }, dispatch);
}

export default connect(null, matchDispatchToProps)(WishlistItem);
