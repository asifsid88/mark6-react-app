import React from 'react';
import {Component} from 'react';
import {Redirect} from 'react-router';

class NoPageFound extends Component {

    handleContinueShopping() {
        window.location = "/";
    }

    render() {
        return (
            <div className="pure-g card catalog-container">
                <div className="pure-u-md-1-2 page-not-found-img">
                    <img src="/static/img/page-not-found.png" title="Page Not Found" alt="Page Not Found" />
                </div>
                <div className="pure-u-md-1-2 page-not-found-error-wrapper">
                    <div className="page-not-found">
                        <h1>Sorry! Page not Found.</h1>
                        <p>Unfortunately the page you are looking for has been moved or deleted.</p>
                        <button onClick={() => this.handleContinueShopping()} className="buy-now-button page-not-found-btn">Go to Home Page</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default NoPageFound;