import React from 'react';
import {Component} from 'react';
import ErrorLabel from './address-form-error-label';
import {validateForm} from '../form-validator/address-form-validator';
import {isStringNullOrEmpty} from '../utility';

class AddAddress extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hasChanged : false,
            data: {
                id: this.props.address.id,
                isDefault: this.props.address.default,
                addressType: this.props.address.addressType || "HOME",
                contactPerson: this.props.address.contactPerson,
                phone: this.props.address.phone,
                pincode: this.props.address.pincode,
                street2: this.props.address.street2,
                street1: this.props.address.street1,
                city: this.props.address.city,
                state: this.props.address.state,
                landmark: this.props.address.landmark,
                alternatePhone: this.props.address.alternatePhone
            },
            errorField: {
                contactPerson: "",
                phone: "",
                pincode: "",
                street2: "",
                street1: "",
                city: "",
                state: "",
                landmark: "",
                alternatePhone: ""
            }
        }
    }

    handleChange(e) {
        this.state.hasChanged = true;
        const field = e.target;
        const success = validateForm(field);

        var errorField = this.state.errorField;
        if(success) {
            errorField[field.name] = "";
        } else {
            errorField[field.name] = "Please fill out this field.";
        }

        var data = this.state.data;
        data[field.name] = field.value;
        this.setState({data: data, errorField: errorField});
    }

    getErrorLabel(name) {
        if(!isStringNullOrEmpty(this.state.errorField[name])) {
            return (
                <ErrorLabel
                    text={this.state.errorField[name]}
                 />
            )
        }
    }

    handleCheckbox(field, value) {
        var data = this.state.data;
        data[field] = value;
        this.setState({data: data, hasChanged: true});
    }

    getCheckbox(field, label) {
        const isChecked = this.state.data.addressType == label;
        let labelCss = "input-radio-label";
        labelCss += isChecked ? " label-checked" : "";
        return (
            <label htmlFor={label} className={labelCss} onClick={() => this.handleCheckbox(field, label)}>
                <input type="radio" className="input-radio" name={field} defaultValue={label} checked={isChecked} />
                <div className="radio-checked"></div>
                <div className="input-radio-text">
                    <span>{label}</span>
                </div>
            </label>
        )
    }

    submitForm(e) {
        e.preventDefault();

        let isValid = true;
        for (var key in this.state.errorField) {
            if (this.state.errorField.hasOwnProperty(key)) {
                if(!isStringNullOrEmpty(this.state.errorField[key])) {
                    isValid = false;
                    break;
                }
            }
        }
        if(!isValid) {
            return;
        }

        if(this.state.hasChanged) {
            this.props.saveAddress(this.state.data);
            this.props.confirmComponent(this.props.index, this.props.address.id, false);
        } else {
            this.props.confirmComponent(this.props.index, this.props.address.id, true);
        }
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="address-wrapper">
                    <form onSubmit={(e) => this.submitForm(e)}>
                        <span className="address-title">{this.props.headingText}</span>
                        <div className="address-field-container">
                            <div className="field-row-wrapper">
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="contactPerson" required="required" autoComplete="contactPerson" tabIndex="1" defaultValue={this.props.address.contactPerson} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="contactPerson" className="field-label">Name</label>
                                    {this.getErrorLabel("contactPerson")}
                                </div>
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="phone" required="required" maxLength="10" autoComplete="phone" tabIndex="2" defaultValue={this.props.address.phone} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="phone" className="field-label">Phone Number</label>
                                    {this.getErrorLabel("phone")}
                                </div>
                            </div>

                            <div className="field-row-wrapper">
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="pincode" required="required" maxLength="6" autoComplete="pincode" tabIndex="3" defaultValue={this.props.address.pincode} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="pincode" className="field-label">Pincode</label>
                                    {this.getErrorLabel("pincode")}
                                </div>
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="street2" tabIndex="4" defaultValue={this.props.address.street2} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="addressLine2" className="field-label">Locality</label>
                                    {this.getErrorLabel("street2")}
                                </div>
                            </div>

                            <div className="field-row-wrapper">
                                <div className="field-row-text-area">
                                    <textarea className="field-input-text-area" name="street1" rows="4" cols="10" tabIndex="5" required="required" autoComplete="street-address" defaultValue={this.props.address.street1} onChange={(e) => this.handleChange(e)}></textarea>
                                    <label htmlFor="addressLine1" className="field-label">Address (Area and Street)</label>
                                    {this.getErrorLabel("street1")}
                                </div>
                            </div>

                            <div className="field-row-wrapper">
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="city" required="required" tabIndex="6" defaultValue={this.props.address.city} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="city" className="field-label">City/District/Town</label>
                                    {this.getErrorLabel("city")}
                                </div>
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="state" required="required" tabIndex="7" defaultValue={this.props.address.state} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="state" className="field-label">State</label>
                                    {this.getErrorLabel("state")}
                                </div>
                            </div>

                            <div className="field-row-wrapper">
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="landmark" required="required" autoComplete="off" tabIndex="8" defaultValue={this.props.address.landmark} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="landmark" className="field-label">Landmark</label>
                                    {this.getErrorLabel("landmark")}
                                </div>
                                <div className="field-row">
                                    <input type="text" className="field-input-text" name="alternatePhone" autoComplete="off" tabIndex="9" maxLength="10" defaultValue={this.props.address.alternatePhone} onChange={(e) => this.handleChange(e)} />
                                    <label htmlFor="alternatePhone" className="field-label">Alternate Phone (Optional)</label>
                                    {this.getErrorLabel("alternatePhone")}
                                </div>
                            </div>

                            <div className="checkout-delivery-address-type-wrapper">
                                <p className="checkout-delivery-address-type">Address Type</p>
                                <div className="checkout-delivery-address-type-field-row">
                                    <div>
                                        {this.getCheckbox("addressType", "HOME")}
                                        {this.getCheckbox("addressType", "OFFICE")}
                                    </div>
                                </div>
                            </div>

                            <div className="field-button-wrapper field-row-wrapper">
                                <input className="field-button-style btn-save-style" type="submit" tabIndex="10" value={this.props.saveButtonText} />
                                <button className="field-button-style btn-cancel" type="button" tabIndex="11" onClick={() => this.props.onPickAddress(this.props.address.id)}>Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default AddAddress;