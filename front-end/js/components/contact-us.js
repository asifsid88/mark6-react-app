import React from 'react';
import {Component} from 'react';

var leftSpanStyle = {
    marginRight: '80px',
    display: 'inline-block',
    minWidth: '100px'
}

class ContactUs extends Component {
    render() {
        return (
            <div className="information-container card">
                <div className="information-panel">
                    <div className="information-panel-heading">Contact Us</div>
                </div>

                <div>
                    <div className="information-panel-title">
                        Arif Mohammed Siddiqui
                    </div>
                    <div className="information-panel-description">
                        <div style={leftSpanStyle}>Mobile Number</div><div style={leftSpanStyle}>0091 9004 522 900</div>
                        <div className="clear-both"></div>
                        <div style={leftSpanStyle}>eMail</div><div style={leftSpanStyle}>arif@mark6traders.com</div>
                    </div>

                    <div className="information-panel-title">
                        Where are we?
                    </div>
                    <div className="information-panel-description">
                        We will be updating our location on Google Map!
                    </div>
                </div>
            </div>
        )
    }
}

export default ContactUs;