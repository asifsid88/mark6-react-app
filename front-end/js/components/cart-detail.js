import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import CartItem from './cart-item';
import CartButtonPanel from './cart-button-panel';
import LoadingSpinner from './loading-spinner';

class CartDetail extends Component {

    constructor(props) {
        super(props);
    }

    getEmptyCartText() {
        return (
            <div className="cart-item-container">
                There is no Item in your cart.
            </div>
        )
    }

    getCartItems() {
        if(this.props.cart) {
            if(this.props.cart.cartItems.length == 0) {
                return (
                    <div>{this.getEmptyCartText()}</div>
                )
            }

            return (
                this.props.cart.cartItems.map((cartItem) => {
                    return (
                        <CartItem
                            key={cartItem.m6sin}
                            cartItem={cartItem}
                            cartRefId={this.props.cart.id}
                         />
                    )
                })
            )
        } else {
            return (
                <div>{this.getEmptyCartText()}</div>
            )
        }
    }

    getCartButtonPanel() {
        const cartItemCount = this.props.cart ? this.props.cart.cartItems.length : 0;

        return (
            <CartButtonPanel cartItemCount={cartItemCount} />
        )
    }

    render() {
        const cartItemCount = this.props.cart ? this.props.cart.cartItems.length : 0;

        var styleContainer = {};
        if(cartItemCount == 0) {
            styleContainer = {
                marginRight: 0
            }
        }

        return (
            <div className="cart-detail-container" style={styleContainer}>
               <div className="cart-heading">My Cart ({cartItemCount})</div>
               <div>{this.getCartItems()}</div>
               <div>{this.getCartButtonPanel()}</div>
            </div>
        )
    }
}

export default CartDetail;

