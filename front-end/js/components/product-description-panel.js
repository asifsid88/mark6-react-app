import React from 'react';
import {Component} from 'react';
import SizeChart from './size-chart';

class ProductDescriptionPanel extends Component {

    createSizeChart() {
        return (
            <SizeChart sizes={this.props.product.sizes}
                       styleCode={this.props.product.styleCode}
                       showSizeChartImage={this.props.showSizeChartImage}
            />
        )
    }

    render() {
        return (
            <div className="card product-description-container">
                <div className="product-description-name">{this.props.product.title}</div>

                <div className="product-description-rating">
                    <span className="product-description-rating-value">4.5 {String.fromCharCode(parseInt('0x2605', 16))}</span>
                    <span className="product-description-rating-count">(97)</span>
                </div>

                <div className="product-description-price">
                    <div className="product-description-price-selling-price">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.product.price.mrp}</div>
                    <div className="product-description-price-mrp">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.product.price.sellingPrice}</div>
                    <div className="product-description-price-offer">{this.props.product.price.offer}% Off</div>
                </div>

                <div className="product-description-size-chart">
                    {this.createSizeChart()}
                </div>

                <div className="product-description-information">
                    <div className="product-description-title">Style Code</div>
                    <div className="product-description-value">{this.props.product.styleCode}</div>
                </div>

                <div className="product-description-information">
                    <div className="product-description-title">Description</div>
                    <div className="product-description-value" dangerouslySetInnerHTML={{__html: this.props.product.description}}></div>
                </div>

                <div className="product-description-information">
                    <div className="product-description-title">Material &amp; Care</div>
                    <div className="product-description-value" dangerouslySetInnerHTML={{__html: this.props.product.material}}></div>
                </div>
            </div>
        )
    }
}

export default ProductDescriptionPanel;