import React from 'react';
import {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login} from '../actions/action-login';
import {isUserLoggedIn} from '../utility';
import {Redirect} from 'react-router';

class Login extends Component {

    constructor(props) {
        super(props);
    }

    loginUser(fbResponse) {
        this.props.login(fbResponse, this.props.returnUrl);
    }

    handleFbLogin() {
    }

    getOrLabel() {
        return (
            <div className="or-label-container">
                <div className="or-horizontal-line"><hr /></div>
                <div className="or-label-text">OR</div>
                <div className="or-horizontal-line"><hr /></div>
            </div>
        )
    }

    getFBComponent() {
        return (
            <FacebookLogin
               appId={process.env.FACEBOOK_APP_ID}
               autoLoad={false}
               fields="name,email,picture"
               onClick={this.handleFbLogin}
               callback={this.loginUser}
               cssClass="facebook-login-button"
               icon="fa-facebook"
               props={this.props}
            />
        )
    }

    getGoogleComponent() {
        return (
            <FacebookLogin
               appId={process.env.FACEBOOK_APP_ID}
               autoLoad={false}
               fields="name,email,picture"
               onClick={this.handleFbLogin}
               callback={this.loginUser}
               cssClass="facebook-login-button"
               icon="fa-facebook"
               props={this.props}
            />
        )
    }

    render() {
        if(!isUserLoggedIn()) {
            return (
                <div className="login-container">
                    <div className="pure-u-md-1-2 login-left-panel">
                        <div className="login-title">Login</div>
                        <div className="login-description">Get access to your Orders, <br/>Wishlist and <br/>Recommendations.</div>
                    </div>
                    <div className="pure-u-md-1-2 login-right-panel">
                        {this.getFBComponent()}
                        {this.getOrLabel()}
                        {this.getGoogleComponent()}
                    </div>
                </div>
            )
        } else {
            return (
                <Redirect to='/' />
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        loggedinUser: state.loggedinUser
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ login }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Login);