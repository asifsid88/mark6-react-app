import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import LoadingSpinner from './loading-spinner';
import {Redirect} from 'react-router';

export default function requireAuth(AuthorizedComponent) {
    class Authenticate extends Component {
        render() {
            if(this.props.userLoggedIn != null) {
                const returnUrl = "ret=" + encodeURIComponent(window.location.href);
                return this.props.userLoggedIn ?
                        <AuthorizedComponent { ...this.props } /> :
                        <Redirect to={{
                            pathname: "/login",
                            search: returnUrl
                        }} />
            } else {
                return <LoadingSpinner />
            }
        }
    }

    function mapStateToProps(state) {
        return {
            userLoggedIn: state.userLoggedIn
        }
    }

    return connect(mapStateToProps, null)(Authenticate);
}