import React from 'react';
import {Component} from 'react';
import {validateAddressForm} from '../form-validator/address-form-validator';
import {isStringNullOrEmpty} from '../utility';

class ErrorLabel extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <span className="error-label">{this.props.text}</span>
        )
    }
}

export default ErrorLabel;