import React from 'react';
import {Component} from 'react';
import Login from './login';

class LoginPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="pure-g card login-page-container">
                <Login />
            </div>
        )
    }
}

export default LoginPage;