import React from 'react';
import {Component} from 'react';
import ProductImagePanel from './product-image-panel';
import ProductDescriptionPanel from './product-description-panel';
import LoadingSpinner from './loading-spinner';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getProductById} from '../actions/action-product';
import {getCustomerDetail} from '../actions/action-get-customer-detail';
import {removeWishlistItem} from '../actions/action-remove-wishlist-item';
import {addToWishlist} from '../actions/action-add-to-wishlist';
import * as QueryString from 'query-string';
import {isUserLoggedIn, getCurrentUrl} from '../utility';
import Login from './login';
import SizeChartImage from './size-chart-image';
import Popup from './pop-up';

class Product extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const queryParams = QueryString.parse(this.props.location.search);
        this.props.getProductById(queryParams.pid);
    }

    addToWishlist() {
        this.props.addToWishlist(this.props.product.id);
    }

    removeWishlistItem() {
        this.props.removeWishlistItem(this.props.product.id);
    }

    createProductImagePanel() {
        let isWishlistItem = this.props.customer ? this.props.customer.wishlist.includes(this.props.product.id) : false;

        return (
            <ProductImagePanel
                thumbnail={this.props.product.thumbnails}
                addToWishlist={this.addToWishlist.bind(this)}
                removeWishlistItem={this.removeWishlistItem.bind(this)}
                isWishlistItem={isWishlistItem}
                showPopup={this.showPopup.bind(this)}
             />
        )
    }

    createProductDescriptionPanel() {
        return (
            <ProductDescriptionPanel
                product={this.props.product}
                showSizeChartImage={this.showSizeChartImage.bind(this)}
             />
        )
    }

    getPopupComponent() {
        if(!isUserLoggedIn()) {
            return (
                <Popup component={Login} ref="popup" returnUrl={getCurrentUrl()} />
            )
        }
    }

    showPopup() {
        this.refs.popup.show();
    }

    getSizeChartImageComponent() {
        return (
            <Popup component={SizeChartImage} ref="sizechart" dialogStyles={{marginTop: "-266px", height: "auto"}} />
        )
    }

    showSizeChartImage() {
        this.refs.sizechart.show();
    }

    render() {
        if(this.props.userLoggedIn) {
            if(!this.props.customer && !this.hasCustomerDetailRequested) {
                this.props.getCustomerDetail();
                this.hasCustomerDetailRequested = true;
            }
        }

        if(this.props.product) {
            return (
                <div className="pure-g product-container">
                    <div className="pure-u-md-5-12">
                        <div className="pure-u-1 product-image-panel">
                            {this.createProductImagePanel()}
                        </div>
                    </div>
                    <div className="pure-u-md-7-12 product-description-panel">
                        {this.createProductDescriptionPanel()}
                    </div>
                    {this.getPopupComponent()}
                    {this.getSizeChartImageComponent()}
                </div>
            )
        } else {
            return (
                <div className="pure-g product-container">
                    <LoadingSpinner />
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        product: state.product,
        customer: state.customer,
        userLoggedIn: state.userLoggedIn
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getProductById, getCustomerDetail, addToWishlist, removeWishlistItem }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Product);