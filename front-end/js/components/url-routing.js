import React from 'react';
import {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import ContactUs from './contact-us';
import AboutUs from './about-us';
import HomePage from './home-page';
import Catalog from './catalog';
import Product from './product';
import Account from './account';
import AccountAddress from './account-address';
import Orders from './orders';
import OrderDetail from './order-details';
import Wishlist from './wishlist';
import Subscriptions from './subscriptions';
import Logout from './logout';
import LoginPage from './login-page';
import Cart from './cart';
import Checkout from './checkout';
import NoPageFound from './no-page-found';
import requireAuth from './authenticate';

class URLRouting extends Component {

    render() {
        return(
            <div className="container-wrapper">
                <Switch>
                    <Route exact path='/' component={HomePage} />
                    <Route exact path='/catalog' component={Catalog} />
                    <Route exact path='/about-us' component={AboutUs} />
                    <Route exact path='/contact-us' component={ContactUs} />
                    <Route exact path='/:productName/p/:productId' component={Product} />
                    <Route exact path='/cart' component={Cart} />
                    <Route exact path="/order_details" component={OrderDetail} />
                    <Route exact path="/login" component={LoginPage} />
                    <Route exact path='/account/logout' component={Logout} />

                    {/* Restricted routes */}
                    <Route exact path='/account/home' component={requireAuth(Account)} />
                    <Route exact path='/account/orders' component={requireAuth(Orders)} />
                    <Route exact path='/account/wishlist' component={requireAuth(Wishlist)} />
                    <Route exact path='/account/subscriptions' component={requireAuth(Subscriptions)} />
                    <Route exact path='/account/addresses' component={requireAuth(AccountAddress)} />
                    <Route exact path='/checkout/init' component={requireAuth(Checkout)} />

                    <Route component={NoPageFound} />
                </Switch>
            </div>
        )
    }
}

export default URLRouting;