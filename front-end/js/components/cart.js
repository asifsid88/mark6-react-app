import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getCart} from '../actions/action-get-cart';
import CartDetail from './cart-detail';
import CartPriceDetail from './cart-price-detail';

class Cart extends Component {

    constructor(props) {
        super(props);
    }

    getCartDetail() {
        return (
            <CartDetail cart={this.props.cart} />
        )
    }

    getCartPriceDetail() {
        return (
            <CartPriceDetail amountPayable={this.props.cart.amountPayable} />
        )
    }

    render() {
        if(this.props.cart &&
            this.props.cart.amountPayable &&
            this.props.cart.amountPayable.totalAmountPayable != 0) {

            return (
                <div className="pure-g cart-container">
                    <div className="pure-u-md-17-24">
                        {this.getCartDetail()}
                    </div>

                    <div className="pure-u-md-7-24">
                        {this.getCartPriceDetail()}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="pure-g cart-container">
                    <div className="pure-u-md-1">
                        {this.getCartDetail()}
                    </div>
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        cart: state.cart
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCart }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Cart);