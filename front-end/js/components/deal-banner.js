import React from 'react';
import {Component} from 'react';

class DealBanner extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="pure-g information-container card">
                <div className="pure-u-md-1-4" style={styles.leftPanel.container}>
                    <div style={styles.leftPanel.wrapper}>
                        <h2 style={styles.leftPanel.title}>{this.props.type}</h2>
                        <div style={styles.leftPanel.button.wrapper}>
                            <div style={styles.leftPanel.button.style}>
                                Button
                            </div>
                        </div>
                    </div>
                </div>
                <div className="pure-u-md-3-4" style={styles.rightPanel.container}>
                    <div style={styles.rightPanel.wrapper}>
                        <div style={styles.rightPanel.product}>Here are the products</div>
                        <div style={styles.rightPanel.leftButton.container}>
                            <div style={styles.rightPanel.leftButton.wrapper}>
                                <svg width="14.6" height="24" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#000"></path>
                                </svg>
                            </div>
                        </div>
                        <div style={styles.rightPanel.rightButton.container}>
                            <div style={styles.rightPanel.rightButton.wrapper}>
                                <svg width="14.6" height="24" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#000"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const styles = {
    leftPanel: {
        container: {
            width: '230px'
        },
        wrapper: {
            padding: '28px 10px',
            textAlign: 'center',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            paddingBottom: '124px',
        },
        title: {
            wordWrap: 'break-word',
            fontSize: '30px',
            lineHeight: '1.38',
            fontWeight: '400',
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif'
        },
        button: {
            wrapper: {
                marginTop: '24px',
                textAlign: 'center'
            },
            style: {
                background: '#348b9f',
                color: '#fff',
                boxShadow: '0 2px 4px 0 rgba(0, 0, 0, .2)',
                border: 'none',
                display: 'inline-block',
                borderRadius: '2px',
                padding: '10px 20px',
                fontSize: '13px',
                fontWeight: '500',
                transition: 'box-shadow 0.2s ease',
                verticalAlign: 'super',
                cursor: 'pointer',
                outline: 'none',
                fontFamily: 'Roboto, Helvetica, Arial, sans-serif'
            }
        }
    },
    rightPanel: {
        container: {
            width: 'calc(100% - 230px)'
        },
        wrapper: {
            overflow: 'hidden',
            position: 'relative',
            width: '100%'
        },
        product: {

        },
        leftButton: {
            container: {
                left: '0',
                position: 'absolute',
                height: '100%',
                top: '0',
                cursor: 'pointer',
                backgroundColor: 'rgba(0, 0, 0, 0)'
            },
            wrapper: {
                opacity: '0.1',
                left: '0',
                borderRadius: '0 4px 4px 0',
                position: 'relative',
                padding: '16px',
                fontSize: '41px',
                top: '50%',
                marginTop: '-49px',
                boxShadow: '1px 2px 10px -1px rgba(0, 0, 0, .3)',
                backgroundColor: 'rgba(255, 255, 255, .98)',
                cursor: 'pointer'
            }
        },
        rightButton: {
            container: {
                right: '0',
                position: 'absolute',
                height: '100%',
                top: '0',
                cursor: 'pointer',
                backgroundColor: 'rgba(0, 0, 0, 0)'
            },
            wrapper: {
                right: '0',
                borderRadius: '4px 0 0 4px',
                position: 'relative',
                padding: '16px',
                fontSize: '41px',
                top: '50%',
                marginTop: '-49px',
                boxShadow: '1px 2px 10px -1px rgba(0, 0, 0, .3)',
                backgroundColor: 'rgba(255, 255, 255, .98)',
                cursor: 'pointer'
            }
        }
    }
}

export default DealBanner;