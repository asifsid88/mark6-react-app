import React from 'react';
import {Component} from 'react';
import OrderCartItemDetail from './order-cart-item-detail';
import OrderTotalPanel from './order-total-panel';

class OrderCartItemPanel extends Component {

    getItemPanel() {
        return (
            this.props.cartItems.map((cartItem) => {
                return (
                    <OrderCartItemDetail
                        cartItem={cartItem}
                        orderDate={this.props.orderDate}
                        key={cartItem.m6sin}
                    />
                )
            })
        )
    }

    getTotalPanel() {
        return (
            <OrderTotalPanel
                amount={this.props.amount}
                orderDate={this.props.orderDate}
            />
        )
    }

    render() {
        return (
            <div className="order-cart-item-panel">
                {this.getItemPanel()}
                {this.getTotalPanel()}
            </div>
        )
    }
}

export default OrderCartItemPanel;