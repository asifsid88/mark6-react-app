import React from 'react';
import {Component} from 'react';
import TopLinkBar from './top-link-bar';
import MenuBar from './menu-bar';

class Header extends Component {
    render() {
        return (
            <div className="header-wrapper">
                <TopLinkBar />
                <MenuBar />
            </div>
        )
    }
}

export default Header;
