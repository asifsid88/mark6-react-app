import React from 'react';
import {Component} from 'react';

class CheckoutPaymentOption extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="checkout-component-wrapper">
                    <h3 className="checkout-component-heading">
                        <span className="checkout-component-heading-index">{this.props.index}</span>
                        <span className="checkout-component-heading-text">{this.props.heading}</span>
                    </h3>
                    <div className="checkout-component-detail">
                        <div className="checkout-component-payment-method-container">
                            RazorPay
                            <div>
                                <span className="payment-option-message">You will be redirected to RazorPay page, where you can use credit/debit card or net-banking as a payment method.</span>
                                <div className="payment-method-continue-button-wrapper">
                                    <button className="checkout-login-action-panel-row-button payment-method-continue-button" type="button" onClick={() => this.props.placeOrder()}>
                                        <span>CONTINUE</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CheckoutPaymentOption;