import React from 'react';
import {Component} from 'react';
import CartItem from './cart-item';
import OrderSummaryActionPanel from './checkout-order-summary-action-panel';

class CheckoutOrderSummary extends Component {

    constructor(props) {
        super(props);
    }

    getCartItems() {
        if(this.props.cart) {
            if(this.props.cart.cartItems.length == 0) {
                return (
                    <div>{this.getEmptyCartText()}</div>
                )
            }

            return (
                this.props.cart.cartItems.map((cartItem) => {
                    return (
                        <CartItem
                            key={cartItem.m6sin}
                            cartItem={cartItem}
                            cartRefId={this.props.cart.id}
                         />
                    )
                })
            )
        } else {
            return (
                <div>{this.getEmptyCartText()}</div>
            )
        }
    }

    getEmptyCartText() {
        return (
            <div className="cart-item-container">
                There is no Item in your cart.
            </div>
        )
    }

    getOrderSummaryActionPanel() {
        const itemCount = this.props.cart ? this.props.cart.cartItems.length : 0;

        return (
            <OrderSummaryActionPanel
                index={this.props.index}
                email={this.props.email}
                itemCount={itemCount}
                confirmComponent={this.props.confirmComponent}
            />
        )
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="checkout-component-wrapper">
                    <h3 className="checkout-component-heading">
                        <span className="checkout-component-heading-index">{this.props.index}</span>
                        <span className="checkout-component-heading-text">{this.props.heading}</span>
                    </h3>
                    <div className="checkout-component-detail">
                        {this.getCartItems()}
                        {this.getOrderSummaryActionPanel()}
                    </div>
                </div>
            </div>
        )
    }
}

export default CheckoutOrderSummary;