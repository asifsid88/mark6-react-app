import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addToCart} from '../actions/action-add-to-cart';
import {Link} from 'react-router-dom';
import {isUserLoggedIn} from '../utility';

class ProductButtonPanel extends Component {

    isProductAlreadyInCart() {
        let isPresent = false;
        if(this.props.m6sin && this.props.cart && this.props.cart.cartItems) {
            const m6sin = this.props.m6sin.replace("::", "");
            this.props.cart.cartItems.some((cartItem) => {
                if(cartItem.m6sin == m6sin) {
                    isPresent = true;
                    return true;
                }
            });
        }

        return isPresent;
    }

    getAddToCartButton() {
        if(!this.isProductAlreadyInCart()) {
            return (
                <div className="add-to-cart-button" onClick={() => this.props.addToCart(this.props.m6sin, '1')}>
                    <svg className="product-cart-icon" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                        fill="#fff"></path>
                    </svg>
                    Add to Cart
                </div>
            )
        } else {
            return (
                <Link to="/cart">
                <div className="add-to-cart-button-disabled">
                    <svg className="product-cart-icon" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                        fill="#fff"></path>
                    </svg>
                    Go to Cart
                </div>
                </Link>
            )
        }
    }

    getBuyNowButton() {
        if(isUserLoggedIn()) {
            if(this.props.m6sin) {
                 return (
                     <form action="/checkout/init" method="GET">
                         <input type="submit" className="buy-now-button" value="Buy Now" />
                         <input type="hidden" value={this.props.m6sin} readOnly name="productId" />
                     </form>
                 )
            }
        } else {
            return (
                <input type="button" className="buy-now-button" value="Buy Now" onClick={() => this.props.showPopup()} />
            )
        }
    }

    render() {
        return (
            <div className="product-button-container">
                <div className="pure-u-md-1-2">
                    {this.getAddToCartButton()}
                </div>

                <div className="pure-u-md-1-2">
                    {this.getBuyNowButton()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        cart: state.cart,
        m6sin: state.m6sin
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ addToCart }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ProductButtonPanel);