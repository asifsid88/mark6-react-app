import React from 'react';
import {Component} from 'react';
import ErrorLabel from './address-form-error-label';
import {validateForm} from '../form-validator/address-form-validator';
import {isStringNullOrEmpty} from '../utility';

class AccountDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: {
                name: this.props.customer.name,
                gender: this.props.customer.gender,
                email: this.props.customer.email,
                phone: this.props.customer.phone
            },
            editable: {
                name: false,
                gender: false,
                email: false,
                phone: false
            },
            errorField: {
                name: "",
                gender: "",
                email: "",
                phone: ""
            }
        }
    }

    handleChange(e) {
        const field = e.target;
        const success = validateForm(field);

        var errorField = this.state.errorField;
        if(success) {
            errorField[field.name] = "";
        } else {
            errorField[field.name] = "Please fill out this field.";
        }

        var data = this.state.data;
        data[field.name] = field.value;
        this.setState({data: data, errorField: errorField});
    }

    getErrorLabel(name) {
        if(!isStringNullOrEmpty(this.state.errorField[name])) {
            return (
                <ErrorLabel
                    text={this.state.errorField[name]}
                 />
            )
        }
    }

    submitForm(e, ...fields) {
        e.preventDefault();

        let isValid = true;
        fields.forEach((field) => {
            if(this.state.errorField.hasOwnProperty(field)) {
                if(!isStringNullOrEmpty(this.state.errorField[field])) {
                    isValid = false;
                }
            }
        })
        if(!isValid) {
            return;
        }

        var data = {};
        fields.forEach((field) => {
            data[field] = this.state.data[field];
        })

        this.handleEnableEdit(...fields);
        this.props.updateCustomerDetail(data);
    }

    getSaveButton(field) {
        if(this.fieldIsEditable(field)) {
            return (
                <div className="field-row">
                    <input className="field-button-style btn-save-style" type="submit" value="Save" />
                </div>
            )
        }
    }

    getEditCancelText(field) {
        if(!this.fieldIsEditable(field)) {
            return (
                <span>Edit</span>
            )
        } else {
            return (
                <span>Cancel</span>
            )
        }
    }

    handleEnableEdit(...fields) {
        var editable = this.state.editable;
        fields.forEach((field) => {
            editable[field] = !editable[field];
        })

        this.setState({editable: editable});
    }

    fieldIsEditable(field) {
        return (this.state.editable[field])
    }

    getInputField(field, fieldLabel, fieldDefaultValue) {
        if(this.fieldIsEditable(field)) {
            return (
                <div className="field-row">
                    <input type="text" className="field-input-text" name={field} required="required" defaultValue={fieldDefaultValue} onChange={(e) => this.handleChange(e)} />
                    <label htmlFor={field} className="field-label">{fieldLabel}</label>
                    {this.getErrorLabel(field)}
                </div>
            )
        } else {
            return (
                <div className="field-row">
                    <input type="text" disabled className="field-input-text" name={field} required="required" defaultValue={fieldDefaultValue} onChange={(e) => this.handleChange(e)} />
                    <label htmlFor={field} className="field-label">{fieldLabel}</label>
                </div>
            )
        }
    }

    handleCheckbox(field, value) {
        var data = this.state.data;
        data[field] = value;
        this.setState({data: data});
    }

    getCheckbox(field, label) {
        const isChecked = this.state.data.gender == label;
        let labelCss = "input-radio-label";
        labelCss += isChecked ? " label-checked" : "";
        if(!this.fieldIsEditable(field)) {
            labelCss += " input-radio-disable";
            return (
                <label htmlFor={label} className={labelCss}>
                    <input type="radio" className="input-radio" name={field} defaultValue={label} checked={isChecked} readOnly />
                    <div className="radio-checked"></div>
                    <div className="input-radio-text">
                        <span>{label}</span>
                    </div>
                </label>
            )
        } else {
            return (
                <label htmlFor={label} className={labelCss} onClick={() => this.handleCheckbox(field, label)}>
                    <input type="radio" className="input-radio" name={field} defaultValue={label} checked={isChecked} />
                    <div className="radio-checked"></div>
                    <div className="input-radio-text">
                        <span>{label}</span>
                    </div>
                </label>
            )
        }
    }

    render() {
        var FIELD_NAME = "name",
              FIELD_GENDER = "gender",
              FIELD_EMAIL = "email",
              FIELD_PHONE = "phone";
        return (
            <div className="card account-detail-container">
                <div className="account-detail-section">
                    <div className="account-detail-title-wrapper">
                        <span className="account-detail-title">Personal Information</span>
                        <span className="account-detail-title-action" onClick={() => this.handleEnableEdit(FIELD_NAME, FIELD_GENDER)}>{this.getEditCancelText(FIELD_NAME)}</span>
                    </div>

                    <form onSubmit={(e) => this.submitForm(e, FIELD_NAME, FIELD_GENDER)}>
                        <div className="field-row-wrapper field-row-width">
                            {this.getInputField(FIELD_NAME, "Name", this.props.customer.name)}
                            {this.getSaveButton(FIELD_NAME)}
                        </div>
                        <div className="account-detail-sub-title">Your Gender</div>
                        <div>
                            {this.getCheckbox(FIELD_GENDER, "Male")}
                            {this.getCheckbox(FIELD_GENDER, "Female")}
                        </div>
                    </form>
                </div>

                <div className="account-detail-section">
                    <div className="account-detail-title-wrapper">
                        <span className="account-detail-title">Mobile Number</span>
                        <span className="account-detail-title-action" onClick={() => this.handleEnableEdit(FIELD_PHONE)}>{this.getEditCancelText(FIELD_PHONE)}</span>
                    </div>
                    <form onSubmit={(e) => this.submitForm(e, FIELD_PHONE)}>
                        <div className="field-row-wrapper field-row-width">
                            {this.getInputField(FIELD_PHONE, "Mobile Number", this.props.customer.phone)}
                            {this.getSaveButton(FIELD_PHONE)}
                        </div>
                    </form>
                </div>

                <div className="account-detail-section">
                    <div className="account-detail-title-wrapper">
                        <span className="account-detail-title">Email Address</span>
                        <span className="account-detail-title-action information-label">You have logged in using facebook account.</span>
                        {/*<span className="account-detail-title-action" onClick={() => this.handleEnableEdit(FIELD_EMAIL)}>{this.getEditCancelText(FIELD_EMAIL)}</span>
                        <span className="account-detail-title-action">Change Password</span>*/}
                    </div>
                    <form onSubmit={(e) => this.submitForm(e, FIELD_EMAIL)}>
                        <div className="field-row-wrapper field-row-width">
                            {this.getInputField(FIELD_EMAIL, "Email Address", this.props.customer.email)}
                            {this.getSaveButton(FIELD_EMAIL)}
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default AccountDetail;