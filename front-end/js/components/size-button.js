import React from 'react';
import {Component} from 'react';

class SizeButton extends Component {

    render() {
        let cssSelected = "size";
        if(this.props.isSelected) {
            cssSelected += " size-selected";
        }

        return (
            <div className={cssSelected} onClick={() => this.props.onClick()}>{this.props.size}</div>
        )
    }
}

export default SizeButton;