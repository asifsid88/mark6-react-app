import React from 'react';
import {Component} from 'react';
import ImageThumbnail from './image-thumbnail';
import ProductButtonPanel from './product-button-panel';
import {isUserLoggedIn, isStringNullOrEmpty} from '../utility';

class ProductImagePanel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentImageUrl: ""
        }
    }

    createImageThumbnail() {
        return(
            this.props.thumbnail.map((thumbnail) => {
                return (
                    <ImageThumbnail
                        key={thumbnail.imageUrl}
                        imageUrl={thumbnail.imageUrl}
                        selectImage={this.handleSelectImage.bind(this)}
                     />
                )
            })
        )
    }

    handleSelectImage(imageUrl) {
        if(this.state.currentImageUrl != imageUrl) {
            this.setState({currentImageUrl : imageUrl});
        }
    }

    createProductButtonPanel() {
        return (
            <ProductButtonPanel
                showPopup={this.props.showPopup}
            />
        )
    }

    getWishlistIcon() {
        if(isUserLoggedIn()) {
            return (
                this.getWishlist()
            )
        } else {
            return (
                <div className="product-wishlist-icon-container" onClick={() => this.props.showPopup()}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#c2c2c2"></path>
                        </svg>
                    </div>
                </div>
            )
        }
    }

    getWishlist() {
        if(this.props.isWishlistItem) {
            return (
                <div className="product-wishlist-icon-container" onClick={() => this.props.removeWishlistItem()}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#ff6161"></path>
                        </svg>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="product-wishlist-icon-container" onClick={() => this.props.addToWishlist()}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#c2c2c2"></path>
                        </svg>
                    </div>
                </div>
            )
        }
    }

    render() {
        const imgUrl = isStringNullOrEmpty(this.state.currentImageUrl) ?
                                this.props.thumbnail[0].imageUrl :
                                this.state.currentImageUrl;
        return (
            <div className="card product-image-container">
                <div className="pure-u-md-3-24">
                    {this.createImageThumbnail()}
                </div>

                <div className="pure-u-md-21-24">
                    <div className="pure-u-1 product-image">
                        <img className="pure-img" src={imgUrl} />
                    </div>
                </div>

                {this.getWishlistIcon()}

                <div className="pure-u-1 product-button-panel">
                    {this.createProductButtonPanel()}
                </div>
            </div>
        )
    }
}

export default ProductImagePanel;