import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import * as Util from '../utility';
import SkyLight from 'react-skylight';
import Login from './login';

class Popup extends Component {

    getOverlayStyles() {
        return {
            backgroundColor: 'rgba(0,0,0,0.7)',
            zIndex: 9999
        }
    }

    getDialogStyles() {
        var defaultProperties = {
                                       borderRadius: "5px",
                                       padding: "0px",
                                       zIndex: 10000,
                                       height: "263px",
                                       marginTop: "-150px"
                                };
        var suppliedProperties = this.props.dialogStyles || {};
        return Object.assign({}, defaultProperties, suppliedProperties);
    }

    getCloseButtonStyle() {
        return {
               cursor: "pointer",
               position: "absolute",
               fontSize: "3.8em",
               right: "-47px",
               top: "-28px",
               color: "#fff"
        }
    }

    show() {
        this.loginPopup.show();
    }

    render() {
        let RendererComponent = this.props.component;

        return (
            <SkyLight overlayStyles={this.getOverlayStyles()} dialogStyles={this.getDialogStyles()} closeButtonStyle={this.getCloseButtonStyle()} ref={ref => this.loginPopup = ref}>
                <RendererComponent {...this.props} />
            </SkyLight>
        )
    }
}

export default Popup;