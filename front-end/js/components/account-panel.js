import React from 'react';
import {Component} from 'react';
import AccountInformationPanel from './account-information-panel';
import AccountActionPanel from './account-action-panel';

class AccountPanel extends Component {

    getAccountInformationPanel() {
        return (
            <AccountInformationPanel
                name={this.props.name}
                profilePic={this.props.pictureUrl}
            />
        )
    }

    getAccountActionPanel() {
        return (
            <AccountActionPanel
                tab={this.props.tab}
            />
        )
    }

    render() {
        return (
            <div className="account-panel-container">
                <div className="pure-u-md-1">
                    {this.getAccountInformationPanel()}
                </div>

                <div className="pure-u-md-1">
                    {this.getAccountActionPanel()}
                </div>
            </div>
        )
    }
}

export default AccountPanel;