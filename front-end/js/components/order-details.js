import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingSpinner from './loading-spinner';
import OrderSummary from './order-summary';
import OrderCartItems from './order-cart-items';
import NoOrderFound from './no-order-found';
import {getOrderDetails} from '../actions/action-order-details';
import * as QueryString from 'query-string';

class OrderDetails extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const queryParams = QueryString.parse(this.props.location.search);
        this.props.getOrderDetails(queryParams.orderId);
    }

    getOrderSummary() {
        return (
            <OrderSummary order={this.props.order} />
        )
    }

    getOrderCartItems() {
        return (
            <OrderCartItems
                showTrackPanel={false}
                order={this.props.order}
            />
        )
    }

    render() {
        if(this.props.order) {
            return (
                <div className="pure-g order-detail-container">
                    <div className="pure-u-md-1">
                        {this.getOrderSummary()}
                    </div>

                    <div className="pure-u-md-1">
                        {this.getOrderCartItems()}
                    </div>
                </div>
            )
        } else {
            return (
                <NoOrderFound
                    msg="No such Order found. Shop now to create your Order."
                />
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getOrderDetails }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(OrderDetails);