import React from 'react';
import {Component} from 'react';
import CheckoutLogin from './checkout-login';
import CheckoutDeliveryAddress from './checkout-delivery-address';
import CheckoutOrderSummary from './checkout-order-summary';
import CheckoutPaymentOption from './checkout-payment-option';
import CheckoutCompletedComponent from './checkout-completed-component';
import AddDeliveryAddress from './checkout-add-delivery-address';
import AddAddress from './checkout-delivery-address-edit';
import {getAddressString} from '../utility';

class CheckoutComponents extends Component {

    constructor(props) {
        super(props);

        this.state = {
            components : Array(5).fill(true),
            isDeliveryPanelEnable: false,
            selectedAddress: "",
            addAddress: false,
            pickedAddress: "",
            editAddress: ""
        }
    }

    getCheckoutCompletedComponent(index, heading, title, content, style = {}) {
        return (
            <CheckoutCompletedComponent
                index={index}
                heading={heading}
                title={title}
                content={content}
                editComponent={this.handleEditComponent.bind(this)}
                style={style}
            />
        )
    }

    getLoginComponent() {
        const heading = "Login",
              index = 1;
        if(this.state.components[index] && this.props.checkout.email) {
            return this.getCheckoutCompletedComponent(index, heading, this.props.checkout.name, this.props.checkout.email);
        } else {
            const personalDetail = {
                name: this.props.checkout.name,
                email: this.props.checkout.email
            }

            return(
                <CheckoutLogin
                    index={index}
                    personalDetail={personalDetail}
                    heading={heading}
                    confirmComponent={this.handleConfirmComponent.bind(this)}
                    logout={this.props.logout}
                />
            )
        }
    }

    getDeliveryAddressComponent() {
        const heading = "Delivery Address",
              index = 2,
              style = {
                marginBottom: "0px"
              };
        if(this.state.components[index]) {
            if(!this.props.checkout.deliveryAddress) {
                if(this.props.checkout.email && this.state.components[1]) {
                    this.state.isDeliveryPanelEnable = true;
                    return(
                        <CheckoutDeliveryAddress
                            index={index}
                            heading={heading}
                            confirmComponent={this.handleDeliveryAddressComponent.bind(this)}
                            selectedAddress={this.state.selectedAddress}
                            saveAddress={this.props.handleSaveAddress}
                            onPickAddress={this.handlePickedAddress.bind(this)}
                            clickEditAddress={this.handleEditAddress.bind(this)}
                            pickedAddress={this.state.pickedAddress}
                            editAddress={this.state.editAddress}
                        />
                    )
                } else {
                    this.state.isDeliveryPanelEnable = false;
                    return this.getCheckoutCompletedComponent(index, heading, "", "", style);
                }
            } else if(this.props.checkout.deliveryAddress) {
                this.state.isDeliveryPanelEnable = false;
                return this.getCheckoutCompletedComponent(index, heading,
                                                          this.props.checkout.deliveryAddress.contactPerson,
                                                          getAddressString(this.props.checkout.deliveryAddress));
            } else {
                this.state.isDeliveryPanelEnable = false;
                return this.getCheckoutCompletedComponent(index, heading, "", "", style);
            }
        } else  {
            this.state.isDeliveryPanelEnable = true;
            let selectedAddress = this.state.selectedAddress;
            if(this.props.checkout.deliveryAddress) {
                selectedAddress = this.props.checkout.deliveryAddress.id;
            }

            return(
                <CheckoutDeliveryAddress
                    index={index}
                    heading={heading}
                    confirmComponent={this.handleDeliveryAddressComponent.bind(this)}
                    selectedAddress={selectedAddress}
                    saveAddress={this.props.handleSaveAddress}
                    onPickAddress={this.handlePickedAddress.bind(this)}
                    clickEditAddress={this.handleEditAddress.bind(this)}
                    pickedAddress={this.state.pickedAddress}
                    editAddress={this.state.editAddress}
                />
            )
        }
    }

    getOrderSummaryComponent() {
        const heading = "Order Summary",
              index = 3,
              style = {
                marginBottom: "0px"
              };
        if(this.state.components[index]) {
            if(this.props.checkout.deliveryAddress) {
                let title = this.props.checkout.orderSummary.cartItems.length;
                title += this.props.checkout.orderSummary.cartItems.length <= 1 ? " item" : " items";

                return this.getCheckoutCompletedComponent(index, heading, title, "");
            } else {
                return this.getCheckoutCompletedComponent(index, heading, "", "", style);
            }
        } else {
             return(
                 <CheckoutOrderSummary
                     index={index}
                     cart={this.props.checkout.orderSummary}
                     email={this.props.checkout.email}
                     heading={heading}
                     confirmComponent={this.handleConfirmComponent.bind(this)}
                 />
             )
        }
    }

    getPaymentOptionComponent() {
        const heading = "Payment",
              index = 4,
              style = {
                marginBottom: "0px"
              };
        if(this.state.components[index]) {
            return this.getCheckoutCompletedComponent(index, heading, "", "", style);
        } else {
            return(
                <CheckoutPaymentOption
                    index={index}
                    heading={heading}
                    placeOrder={this.props.placeOrder}
                />
            )
        }
    }

    getAddAddressComponent() {
        if(this.state.isDeliveryPanelEnable) {
            if(this.state.addAddress) {
                const address = {};
                return(
                    <div className="pure-u-md-1 checkout-component">
                        <AddAddress
                            index={this.props.index}
                            address={address}
                            confirmComponent={this.handleDeliveryAddressComponent.bind(this)}
                            onPickAddress={this.handleCancelLinkNewAddAddress.bind(this)}
                            saveAddress={this.props.handleSaveAddress}
                            headingText="Add Address"
                            saveButtonText="Save and Deliver Here"
                        />
                    </div>
                )
            } else {
                return(
                    <div className="pure-u-md-1 checkout-component">
                        <AddDeliveryAddress
                            addNewAddress={this.handleAddNewAddress.bind(this)}
                        />
                    </div>
                )
            }
        }
    }

    handleCancelLinkNewAddAddress(addressId) {
        this.setState({addAddress : false});
    }

    handleAddNewAddress() {
        this.setState({addAddress : true, editAddress : "", pickedAddress : ""});
    }

    handlePickedAddress(addressId) {
        this.setState({pickedAddress : addressId, editAddress : ""});
    }

    handleEditAddress(addressId) {
        this.setState({editAddress : addressId});
    }

    handleEditComponent(index) {
        const components = this.state.components.slice();
        const len = components.length;
        for(var i=1; i<=len; i++) {
            if(i == index) {
                components[i] = false;
            } else {
                components[i] = true;
            }
        }
        this.setState({components : components});
    }

    handleConfirmComponent(index) {
        const components = this.state.components.slice();
        components[index] = true;
        if(index < components.length) {
            components[index+1] = false;
        }

        this.setState({components : components, addAddress : false});
    }

    handleDeliveryAddressComponent(index, addressId, selectAddress = true) {
        if(!this.props.checkout.deliveryAddress || addressId != this.props.checkout.deliveryAddress.id) {
            this.state.selectedAddress = addressId;
            if(selectAddress) {
                this.props.handleSelectAddress(addressId);
            }
        }

        this.handleConfirmComponent(index);
    }

    render() {
        return(
            <div className="pure-g">
                <div className="pure-u-md-1 checkout-component">
                    {this.getLoginComponent()}
                </div>
                <div className="pure-u-md-1 checkout-component">
                    {this.getDeliveryAddressComponent()}
                </div>
                {this.getAddAddressComponent()}
                <div className="pure-u-md-1 checkout-component">
                    {this.getOrderSummaryComponent()}
                </div>
                <div className="pure-u-md-1 checkout-component">
                    {this.getPaymentOptionComponent()}
                </div>
            </div>
        )
    }
}

export default CheckoutComponents;