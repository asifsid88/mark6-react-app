import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingSpinner from './loading-spinner';
import DeliveryAddress from './checkout-delivery-address-module';
import AddAddress from './checkout-delivery-address-edit';
import {getAddressList} from '../actions/action-address-list';

class CheckoutDeliveryAddress extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if(!this.props.addressList) {
            this.props.getAddressList();
        }
    }

    getDeliveryAddressList() {
        if(this.props.addressList) {
            if(this.props.addressList.length == 0) {
                return (
                    <div className="cart-item-container">
                        There is no Address in your profile.
                    </div>
                )
            } else {
                return (
                    this.props.addressList.map((address) => {
                        let isSelected = false;

                        if(!this.props.pickedAddress && !this.props.selectedAddress) {
                            if(address.default) {
                                isSelected = true;
                            }
                        }

                        if(!this.props.pickedAddress && this.props.selectedAddress) {
                            if(this.props.selectedAddress == address.id) {
                                isSelected = true;
                            }
                        }

                        if(this.props.pickedAddress == address.id) {
                            isSelected = true;
                        }

                        if(this.props.editAddress && this.props.editAddress == address.id && isSelected) {
                            return (
                                <AddAddress
                                    index={this.props.index}
                                    key={address.id}
                                    address={address}
                                    confirmComponent={this.props.confirmComponent}
                                    onPickAddress={this.props.onPickAddress}
                                    saveAddress={this.props.saveAddress}
                                    headingText="Edit Address"
                                    saveButtonText="Save and Deliver Here"
                                />
                            )
                        } else {
                            return (
                                <DeliveryAddress
                                    index={this.props.index}
                                    key={address.id}
                                    address={address}
                                    confirmComponent={this.props.confirmComponent}
                                    isSelected={isSelected}
                                    onPickAddress={this.props.onPickAddress}
                                    clickEditAddress={this.props.clickEditAddress}
                                />
                            )
                        }
                    })
                )
            }
        } else {
            return (
                <div className="pure-g checkout-container">
                    <LoadingSpinner />
                </div>
            )
        }
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="checkout-component-wrapper">
                    <h3 className="checkout-component-heading">
                        <span className="checkout-component-heading-index">{this.props.index}</span>
                        <span className="checkout-component-heading-text">{this.props.heading}</span>
                    </h3>
                    <div className="checkout-component-detail">{this.getDeliveryAddressList()}</div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        addressList: state.addressList
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getAddressList }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CheckoutDeliveryAddress);