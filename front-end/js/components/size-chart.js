import React from 'react';
import {Component} from 'react';
import SizeButton from './size-button';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {selectSize} from '../actions/action-select-size';

class SizeChart extends Component {

    constructor() {
        super();
        this.state = {
            sizes: [],
        };
    }

    componentDidMount() {
        this.props.sizes.some((size) => {
            if(size.count != 0) {
                this.handleSizeSelected(size.value);
                return true;
            }
        });
    }

    createSizeButtons() {
        return (
            this.props.sizes.map((size) => {
                if(size.count != 0) {
                    return (<SizeButton
                                key={size.value}
                                isSelected={this.state.sizes[size.value]}
                                size={size.value}
                                onClick={() => this.handleSizeSelected(size.value)}
                            />)
                }
            })
        )
    }

    handleSizeSelected(size) {
        const sizes = this.state.sizes.slice();
        sizes[size] = true;
        this.setState({sizes: sizes});
        this.props.selectSize(this.props.styleCode, size);
    }

    render() {
        return (
            <div>
                <div className="product-size-wrapper select-size">Select Size</div>
                <div className="size-wrapper">
                    {this.createSizeButtons()}
                </div>
                <div className="size-chart-link" onClick={() => this.props.showSizeChartImage()}>See Size Chart </div>
            </div>
        )
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ selectSize }, dispatch);
}

export default connect(null, matchDispatchToProps)(SizeChart);