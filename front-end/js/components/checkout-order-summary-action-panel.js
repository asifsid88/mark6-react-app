import React from 'react';
import {Component} from 'react';
import CartButtonPanel from './cart-button-panel';
import {Link} from 'react-router-dom';

class OrderSummaryActionPanel extends Component {

    constructor(props) {
        super(props);
    }

    getText() {
        if(this.props.itemCount && this.props.itemCount != 0) {
            return (
                <span>Order confirmation email will be sent to
                    <span className="text-bold">
                        &nbsp;{this.props.email}
                    </span>
                </span>
            )
        } else {
            return (
                <span>&nbsp;</span>
            )
        }
    }

    getButton() {
        if(this.props.itemCount && this.props.itemCount != 0) {
            return (
                <span>
                    <button className="checkout-order-summary-action-panel-btn" onClick={() => this.props.confirmComponent(this.props.index)}>Continue</button>
                </span>
            )
        } else {
            return (
                <Link to="/">
                    <button className="checkout-order-summary-action-panel-btn">
                        <svg width="16" height="27" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg" className="cart-button-cont-shopping-arrow">
                            <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z"
                                  fill="#fff"></path>
                        </svg>
                        <span>Continue shopping</span>
                    </button>
                </Link>
            )
        }
    }

    render() {
        return(
            <div className="checkout-order-summary-action-panel-container">
                {this.getText()}
                {this.getButton()}
            </div>
        )
    }
}

export default OrderSummaryActionPanel;