import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';

class AccountActionPanel extends Component {

    isActiveTab(current, selected) {
        if(current == selected) {
            return "account-actions account-action-selected";
        } else {
            return "account-actions";
        }
    }

    render() {
        return (
            <div className="card">
                <div className="account-action-heading">My Stuff</div>
                <div>
                    <Link to="/account/orders">
                        <div className={this.isActiveTab('orders', this.props.tab)}>My Orders</div>
                    </Link>
                    <Link to="/account/wishlist">
                        <div className={this.isActiveTab('wishlist', this.props.tab)}>My Wishlist</div>
                    </Link>
                </div>

                <div className="account-section-divider"></div>

                <div className="account-action-heading">Account Settings</div>
                <div>
                    <Link to="/account/home">
                        <div className={this.isActiveTab('personal', this.props.tab)}>Profile Information</div>
                    </Link>
                    <Link to="/account/addresses">
                        <div className={this.isActiveTab('address', this.props.tab)}>Manage Addresses</div>
                    </Link>
                    <Link to="/account/subscriptions">
                        <div className={this.isActiveTab('subscriptions', this.props.tab)}>Email Preferences</div>
                    </Link>
                </div>
            </div>
        )
    }
}

export default AccountActionPanel;