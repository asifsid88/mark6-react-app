import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import AccountPanel from './account-panel';
import AccountSubscriptionDetail from './account-subscription-detail';
import LoadingSpinner from './loading-spinner';
import NoOrderFound from './no-order-found';
import WishlistDetails from './wishlist-details';
import {getCustomerDetail} from '../actions/action-get-customer-detail';
import {getWishlist} from '../actions/action-get-wishlist';
import {removeWishlistItem} from '../actions/action-remove-wishlist-item';

class Wishlist extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCustomerDetail();
        this.props.getWishlist();
    }

    removeWishlistItem(productId) {
        this.props.removeWishlistItem(productId);
    }

    getAccountPanel() {
        if(this.props.customer) {
            return (
                <AccountPanel
                    name={this.props.customer.name}
                    pictureUrl={this.props.customer.pictureUrl}
                    tab="wishlist"
                />
            )
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    getWishlist() {
        if(this.props.wishlist) {
            return (
                <WishlistDetails
                    wishlist={this.props.wishlist.cartItems}
                    removeWishlistItem={this.removeWishlistItem.bind(this)}
                />
            )
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    render() {
        return (
            <div className="pure-g account-container">
                <div className="pure-u-md-7-24">
                    {this.getAccountPanel()}
                </div>

                <div className="pure-u-md-17-24 account-detail-container-wrapper">
                    {this.getWishlist()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        customer: state.customer,
        wishlist: state.wishlist
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCustomerDetail, getWishlist, removeWishlistItem }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Wishlist);