import React from 'react';
import {Component} from 'react';
import ProductThumbnail from './product-thumbnail';
import LoadingSpinner from './loading-spinner';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getCatalog} from '../actions/action-catalog';
import {getCustomerDetail} from '../actions/action-get-customer-detail';
import {addToWishlist} from '../actions/action-add-to-wishlist';
import {removeWishlistItem} from '../actions/action-remove-wishlist-item';
import {isUserLoggedIn, getCurrentUrl} from '../utility';
import Login from './login';
import Popup from './pop-up';

class Catalog extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCatalog();
    }

    addToWishlist(productId) {
        this.props.addToWishlist(productId);
    }

    removeWishlistItem(productId) {
        this.props.removeWishlistItem(productId);
    }

    createProductThumbnails() {
        let wishlistItems = [];
        if(this.props.customer) {
            wishlistItems = this.props.customer.wishlist;
        }

        return (
            this.props.catalog.products.map((product) => {
                return (
                    <ProductThumbnail
                        key={product.id}
                        product={product}
                        addToWishlist={this.addToWishlist.bind(this)}
                        removeWishlistItem={this.removeWishlistItem.bind(this)}
                        wishlistItems={wishlistItems}
                        showPopup={this.showPopup.bind(this)}
                    />
                )
            })
        )
    }

    getPopupComponent() {
        if(!isUserLoggedIn()) {
            return (
                <Popup component={Login} ref="popup" returnUrl={getCurrentUrl()} />
            )
        }
    }

    showPopup() {
        this.refs.popup.show();
    }

    render() {
        if(this.props.userLoggedIn) {
            if(!this.props.customer && !this.hasCustomerDetailRequested) {
                this.props.getCustomerDetail();
                this.hasCustomerDetailRequested = true;
            }
        }

        if(this.props.catalog) {
            return (
                <div className="pure-g card catalog-container">
                    <div className="pure-u-md-1 catalog-product-thumbnail">
                        {this.createProductThumbnails()}
                    </div>
                    {this.getPopupComponent()}
                </div>
            )
        } else {
            return (
                <div className="pure-g catalog-container">
                    <LoadingSpinner />
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        catalog: state.catalog,
        customer: state.customer,
        userLoggedIn: state.userLoggedIn
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCatalog, getCustomerDetail, addToWishlist, removeWishlistItem }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Catalog);