import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';

class CheckoutLoginActionPanel extends Component {

    constructor(props) {
        super(props);
    }

    getReturnUrl() {
        const url = window.location.protocol + "//" +
                  window.location.hostname + ":" +
                  window.location.port + "/login";

        return (
            encodeURIComponent(url)
        )
    }

    render() {
        return(
            <div className="checkout-login-action-panel">
                <div className="checkout-login-action-panel-row">
                    <span className="checkout-login-action-panel-row-title">Name</span>
                    <span className="checkout-login-action-panel-row-detail">{this.props.personalDetail.name}</span>
                </div>

                <div className="checkout-login-action-panel-row">
                    <span className="checkout-login-action-panel-row-title">Email</span>
                    <span className="checkout-login-action-panel-row-detail">{this.props.personalDetail.email}</span>
                </div>
                <div className="checkout-login-action-panel-row">
                    <a href="#" onClick={(event) => {
                                            event.preventDefault();
                                            this.props.logout(this.getReturnUrl());
                                         }}>
                        <span>Logout &amp; Sign in to another account</span>
                    </a>
                </div>

                <div className="checkout-login-action-panel-row">
                    <button className="checkout-login-action-panel-row-button" onClick={() => this.props.confirmComponent(this.props.index)}>
                        <span>Continue Checkout</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default CheckoutLoginActionPanel;