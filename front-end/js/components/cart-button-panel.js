import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';

class CartButtonPanel extends Component {

    constructor(props) {
        super(props);
    }

    getPlaceOrderButton() {
        if(this.props.cartItemCount != 0) {
            return (
                <form action="/checkout/init" method="GET">
                    <input type="submit" className="cart-button-place-order" value="Place Order" />
                </form>
            )
        }
    }

    render() {
        return (
            <div className="cart-button-panel-container">
                <div className="cart-button-panel-wrapper">
                    <Link to="/">
                        <button className="cart-button-cont-shopping">
                            <svg width="16" height="27" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg" className="cart-button-cont-shopping-arrow">
                                <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#000"></path>
                            </svg>
                        <span>Continue shopping</span>
                        </button>
                    </Link>
                    {this.getPlaceOrderButton()}
                </div>
            </div>
        )
    }
}

export default CartButtonPanel;
