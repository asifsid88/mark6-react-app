import React from 'react';
import {Component} from 'react';

class AddDeliveryAddress extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="checkout-component-container" onClick={() => this.props.addNewAddress()}>
                <div className="checkout-component-wrapper">
                    <div className="checkout-component-cover">
                        <div className="checkout-component-heading-index-plus">
                            <img height="14" width="14" src="/static/img/plus-sign.svg" className="checkout-component-heading-index-plus-img" />
                            Add a new address
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddDeliveryAddress;