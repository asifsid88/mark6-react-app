import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import * as Util from '../utility';
import SkyLight from 'react-skylight';
import Login from './login';
import Popup from './pop-up';

class UserProfileMenuBar extends Component {

    render() {
        if(!Util.isUserLoggedIn()) {
            return(
                <li className="pure-menu-item">
                    <div className="pure-menu-link" onClick={() => this.refs.popup.show()}>Log In</div>
                    <Popup component={Login} ref="popup" />
                </li>
            )
        } else {
            let firstName = 'User';
            if(Util.cookies.hasItem('m6')) {
                firstName = Util.base64Decode(Util.cookies.getItem('m6'));
            }

            return (
                <li className="pure-menu-item user-profile-item">
                    <div className="pure-menu-link user-profile">Hi {firstName}!</div>
                    <ul className="pure-menu-list user-profile-menu">
                        <li className="user-profile-menu-item">
                            <Link to="/account/home" className="pure-menu-link user-profile-menu-link">Account</Link>
                        </li>
                        <li className="user-profile-menu-item">
                            <Link to="/account/orders" className="pure-menu-link user-profile-menu-link">Orders</Link>
                        </li>
                        <li className="user-profile-menu-item">
                            <Link to="/account/wishlist" className="pure-menu-link user-profile-menu-link">Wishlist</Link>
                        </li>
                        <li className="user-profile-menu-item">
                            <Link to="/account/subscriptions" className="pure-menu-link user-profile-menu-link">Email Preferences</Link>
                        </li>
                        <li className="user-profile-menu-item">
                            <Link to="/account/logout" className="pure-menu-link user-profile-menu-link">Logout</Link>
                        </li>
                    </ul>
                </li>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        loggedinUser: state.loggedinUser,
        logoutUser: state.logoutUser
    }
}

export default connect(mapStateToProps)(UserProfileMenuBar);