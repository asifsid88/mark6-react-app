import React from 'react';
import {Component} from 'react';
import CheckoutLoginActionPanel from './checkout-login-action-panel';
import CheckoutLoginBenefitPanel from './checkout-login-benefit-panel';

class CheckoutLoginModule extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="pure-g checkout-login-module">
                <div className="pure-u-md-10-24">
                    <CheckoutLoginActionPanel
                        personalDetail={this.props.personalDetail}
                        confirmComponent={this.props.confirmComponent}
                        index={this.props.index}
                        logout={this.props.logout}
                    />
                </div>

                <div className="pure-u-md-14-24">
                    <CheckoutLoginBenefitPanel />
                </div>
            </div>
        )
    }
}

export default CheckoutLoginModule;