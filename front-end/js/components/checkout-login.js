import React from 'react';
import {Component} from 'react';
import CheckoutLoginModule from './checkout-login-module';

class CheckoutLogin extends Component {

    constructor(props) {
        super(props);
    }

    getLoginModule() {
        return (
            <CheckoutLoginModule
                personalDetail={this.props.personalDetail}
                confirmComponent={this.props.confirmComponent}
                index={this.props.index}
                logout={this.props.logout}
            />
        )
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="checkout-component-wrapper">
                    <h3 className="checkout-component-heading">
                        <span className="checkout-component-heading-index">{this.props.index}</span>
                        <span className="checkout-component-heading-text">{this.props.heading}</span>
                    </h3>
                    <div className="checkout-component-detail">
                        {this.getLoginModule()}
                        <div className="checkout-component-detail-warning-text">
                            <span>
                                Please note that upon clicking "Logout" you will lose all items in cart and will be redirected to Mark6 home page.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CheckoutLogin;