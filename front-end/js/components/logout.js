import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isUserLoggedIn} from '../utility';
import {logout} from '../actions/action-logout';
import LoadingSpinner from './loading-spinner';
import {Redirect} from 'react-router';

class Logout extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.logout();
    }

    render() {
        if(!isUserLoggedIn()) {
            return (
                <Redirect to='/' />
            )
        } else {
            return (
                <div className="pure-g catalog-container">
                    <LoadingSpinner />
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        logoutUser: state.logoutUser
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ logout }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Logout);