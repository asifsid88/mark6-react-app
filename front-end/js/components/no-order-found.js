import React from 'react';
import {Component} from 'react';
import CartButtonPanel from './cart-button-panel';

class NoOrderFound extends Component {

    constructor(props) {
        super(props);
    }

    getEmptyOrderText() {
        return (
            <div className="cart-item-container">
                {this.props.msg}
            </div>
        )
    }

    getCartButtonPanel() {
        return (
            <CartButtonPanel cartItemCount="0" />
        )
    }

    render() {
        var styleContainer = {
                margin: "10px 2%"
            }

        return (
            <div className="cart-detail-container" style={styleContainer}>
               <div className="cart-heading">My Order</div>
               <div>{this.getEmptyOrderText()}</div>
               <div>{this.getCartButtonPanel()}</div>
            </div>
        )
    }
}

export default NoOrderFound;