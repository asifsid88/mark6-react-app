import React from 'react';
import {Component} from 'react';
import SizeButton from './size-button';
import {createProductPageUrl, isUserLoggedIn} from '../utility';

class ProductThumbnail extends Component {

    getWishlistIcon() {
        if(isUserLoggedIn()) {
            return (
                this.getWishlist()
            )
        } else {
            return (
                <div className="catalog-wishlist-icon-container" onClick={() => this.props.showPopup()}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#c2c2c2"></path>
                        </svg>
                    </div>
                </div>
            )
        }
    }

    getWishlist() {
        if(this.props.wishlistItems.includes(this.props.product.id)) {
            return (
                <div className="catalog-wishlist-icon-container" onClick={() => this.props.removeWishlistItem(this.props.product.id)}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#ff6161"></path>
                        </svg>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="catalog-wishlist-icon-container" onClick={() => this.props.addToWishlist(this.props.product.id)}>
                    <div className="wishlist-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 10 18" preserveAspectRatio="xMinYMax meet">
                            <path d="M7.695 15.35C3.06 11.14 0 8.356 0 4.958 0 2.172 2.178 0 4.95 0 6.516 0 9.164 1.764 9 2.91 9.164 1.763 11.484 0 13.05 0 15.822 0 18 2.172 18 4.958c0 3.398-3.06 6.183-7.695 10.392L9 16.54l-1.305-1.19z"
                            fill="#c2c2c2"></path>
                        </svg>
                    </div>
                </div>
            )
        }
    }

    render() {
        const productUrl = createProductPageUrl(this.props.product.id,
                                                this.props.product.styleCode,
                                                this.props.product.title);
        return (
            <div className="pure-u-md-5-24 thumbnail-container">

                <a href={`${productUrl}`}>
                    <div className="product-thumbnail">
                        <img className="pure-img" src={this.props.product.defaultImage.imageUrl} alt={this.props.product.title} title={this.props.product.title} />
                    </div>
                </a>

                {this.getWishlistIcon()}

                <a href={`${productUrl}`}>
                    <div className="product-name">
                        {this.props.product.title}
                    </div>
                </a>

                <div className="product-rating">
                    <span className="product-rating-star">4.5 {String.fromCharCode(parseInt('0x2605', 16))}</span>
                    <span className="product-rating-count">(97)</span>
                </div>

                <a href={`${productUrl}`}>
                    <div className="product-price">
                        <div className="product-selling-price">
                            {String.fromCharCode(parseInt('0x20B9', 16))}{this.props.product.price.sellingPrice}
                        </div>
                        <div className="product-mrp">
                            {String.fromCharCode(parseInt('0x20B9', 16))}{this.props.product.price.mrp}
                        </div>
                        <div className="product-offer">
                            {this.props.product.price.offer}% off
                        </div>
                    </div>
                </a>
            </div>
        )
    }
}

export default ProductThumbnail;