import React from 'react';
import {Component} from 'react';
import OrderTrackPanel from './order-track-panel';
import OrderCartItemPanel from './order-cart-item-panel';

class OrderCartItems extends Component {

    getTrackPanel() {
        if(this.props.showTrackPanel) {
            return (
                <OrderTrackPanel orderId={this.props.order.id} />
            )
        }
    }

    getCartItemPanel() {
        return (
            <OrderCartItemPanel
                cartItems={this.props.order.cartItems}
                amount={this.props.order.amountPayable}
                orderDate={this.props.order.orderCreationDate}
            />
        )
    }

    render() {
        return (
            <div className="order-container">
                {this.getTrackPanel()}
                {this.getCartItemPanel()}
            </div>
        )
    }
}

export default OrderCartItems;