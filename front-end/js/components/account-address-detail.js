import React from 'react';
import {Component} from 'react';
import AddDeliveryAddress from './checkout-add-delivery-address';
import AddAddress from './checkout-delivery-address-edit';
import AddressDescription from './account-address-description-component';
import {getAddressString} from '../utility';

class AccountAddressDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            components : Array(5).fill(true),
            addAddress: false,
            editAddress: ""
        }
    }

    handleAddNewAddress() {
        this.setState({addAddress : true, editAddress : ""});
    }

    handleAddAddressComponent(index, addressId, selectAddress = true) {
        this.handleCancelLinkNewAddAddress(addressId);
    }

    handleCancelLinkNewAddAddress(addressId) {
        this.setState({addAddress : false, editAddress : ""});
    }

    handleEditAddress(addressId) {
        this.setState({editAddress : addressId, addAddress : false});
    }

    getAddAddressComponent() {
        if(this.state.addAddress) {
            const address = {};
            return (
                <div className="pure-u-md-1 account-address-component">
                    <AddAddress
                        index="1"
                        address={address}
                        confirmComponent={this.handleAddAddressComponent.bind(this)}
                        onPickAddress={this.handleCancelLinkNewAddAddress.bind(this)}
                        saveAddress={this.props.saveAddress}
                        headingText="Add Address"
                        saveButtonText="Save"
                    />
                </div>
            )
        } else {
            return(
                <div className="pure-u-md-1 account-address-component">
                    <AddDeliveryAddress
                        addNewAddress={this.handleAddNewAddress.bind(this)}
                    />
                </div>
            )
        }
    }

    getAddressListComponent() {
        if(this.props.addressList && this.props.addressList.length != 0) {
            return (
                this.props.addressList.map((address) => {
                    if(address.id == this.state.editAddress) {
                        return (
                            <div className="address-description-container">
                                <AddAddress
                                    key={address.id}
                                    index="1"
                                    address={address}
                                    confirmComponent={this.handleAddAddressComponent.bind(this)}
                                    onPickAddress={this.handleCancelLinkNewAddAddress.bind(this)}
                                    saveAddress={this.props.saveAddress}
                                    headingText="Edit Address"
                                    saveButtonText="Save"
                                />
                            </div>
                        )
                    } else {
                        return (
                            <AddressDescription
                                key={address.id}
                                address={address}
                                content={getAddressString(address)}
                                editAddress={this.handleEditAddress.bind(this)}
                                setDefaultAddress={this.props.setDefaultAddress}
                                deleteAddress={this.props.deleteAddress}
                            />
                        )
                    }
                })
            )
        }
    }

    render() {
        return (
            <div className="card account-detail-container">
                <div className="account-detail-section">
                    <div className="account-detail-title-wrapper">
                        <span className="account-detail-title">Manage Addresses</span>
                    </div>
                    {this.getAddAddressComponent()}
                    <div>
                        {this.getAddressListComponent()}
                    </div>
                </div>
            </div>
        )
    }
}

export default AccountAddressDetail;