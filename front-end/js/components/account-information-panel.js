import React from 'react';
import {Component} from 'react';
import {isStringNullOrEmpty} from '../utility';

class AccountInformationPanel extends Component {

    getProfilePicture() {
        if(isStringNullOrEmpty(this.props.profilePic)) {
            return (
                <img className="account-information-profile-img" src="/static/img/profile-pic-male.svg" />
            )
        } else {
            return (
                <img className="account-information-profile-img" src={this.props.profilePic} />
            )
        }
    }

    render() {
        return (
            <div className="card account-information-container">
                {this.getProfilePicture()}
                <div className="account-information-profile">
                    <div className="account-information-profile-greet">Hello,</div>
                    <div className="account-information-profile-name">{this.props.name}</div>
                </div>

            </div>
        )
    }
}

export default AccountInformationPanel;