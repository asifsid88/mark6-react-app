import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import AccountPanel from './account-panel';
import AccountAddressDetail from './account-address-detail';
import LoadingSpinner from './loading-spinner';
import {getCustomerDetail} from '../actions/action-get-customer-detail';
import {saveAddress} from '../actions/action-save-address';
import {setDefaultAddress} from '../actions/action-set-default-address';
import {deleteAddress} from '../actions/action-delete-address';

class AccountAddress extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCustomerDetail();
    }

    saveAddress(address) {
        this.props.saveAddress(address);
    }

    setDefaultAddress(addressId, isDefault) {
        if(isDefault) return;
        this.props.setDefaultAddress(addressId);
    }

    deleteAddress(addressId) {
        this.props.deleteAddress(addressId);
    }

    getAccountPanel() {
        if(this.props.customer) {
            return (
                <AccountPanel
                    name={this.props.customer.name}
                    pictureUrl={this.props.customer.pictureUrl}
                    tab="address"
                />
            )
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    getAccountAddressDetail() {
        if(this.props.customer) {
            return (
                <AccountAddressDetail
                    addressList={this.props.customer.addressList}
                    saveAddress={this.props.saveAddress.bind(this)}
                    setDefaultAddress={this.props.setDefaultAddress.bind(this)}
                    deleteAddress={this.props.deleteAddress.bind(this)}
                />
            )
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    render() {
        return (
            <div className="pure-g account-container">
                <div className="pure-u-md-7-24">
                    {this.getAccountPanel()}
                </div>

                <div className="pure-u-md-17-24 account-detail-container-wrapper">
                    {this.getAccountAddressDetail()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        customer: state.customer
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCustomerDetail, saveAddress, setDefaultAddress, deleteAddress }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AccountAddress);