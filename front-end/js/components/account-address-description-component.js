import React from 'react';
import {Component} from 'react';

class AddressDescription extends Component {

    constructor(props) {
        super(props);
    }

    getDefaultTag() {
        if(this.props.address.default) {
            return (
                <span className="address-description-heading-default">DEFAULT</span>
            )
        }
    }

    render() {
        return(
            <div className="address-description-container">
                <div className="address-description-wrapper">
                    <div className="address-description-menu-container">
                        <div className="address-description-menu-wrapper">
                            <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0IiBoZWlnaHQ9IjE2IiB2aWV3Qm94PSIwIDAgNCAxNiI+CiAgICA8ZyBmaWxsPSIjODc4Nzg3IiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxjaXJjbGUgY3g9IjIiIGN5PSIyIiByPSIyIi8+CiAgICAgICAgPGNpcmNsZSBjeD0iMiIgY3k9IjgiIHI9IjIiLz4KICAgICAgICA8Y2lyY2xlIGN4PSIyIiBjeT0iMTQiIHI9IjIiLz4KICAgIDwvZz4KPC9zdmc+Cg==" />
                            <div className="address-description-menu-list">
                                <div className="address-description-menu-item" onClick={() => this.props.editAddress(this.props.address.id)}>
                                    <span>Edit</span>
                                </div>
                                <div className="address-description-menu-item" onClick={() => this.props.setDefaultAddress(this.props.address.id, this.props.address.default)}>
                                    <span>Set as default</span>
                                </div>
                                <div className="address-description-menu-item" onClick={() => this.props.deleteAddress(this.props.address.id)}>
                                    <span>Delete</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="address-description-heading">
                        <span className="address-description-heading-type">{this.props.address.addressType}</span>
                        {this.getDefaultTag()}
                    </div>

                    <p className="address-description-title">
                        <span className="address-description-title-name">{this.props.address.contactPerson}</span>
                        <span className="address-description-title-phone">{this.props.address.phone}</span>
                    </p>

                    <span className="address-description-content">{this.props.content}</span>
                </div>
            </div>
        )
    }
}

export default AddressDescription;