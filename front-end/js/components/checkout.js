import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingSpinner from './loading-spinner';
import {checkoutInit} from '../actions/action-checkout-init';
import {selectAddress} from '../actions/action-select-address';
import {saveAddress} from '../actions/action-save-address';
import {placeOrder} from '../actions/action-place-order';
import {logout} from '../actions/action-logout';
import CartPriceDetail from './cart-price-detail';
import CheckoutComponents from './checkout-components';
import InternalServerError from './internal-server-error';
import * as QueryString from 'query-string';
import {Redirect} from 'react-router';

class Checkout extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const queryParams = QueryString.parse(this.props.location.search);
        this.props.checkoutInit(queryParams.productId);
    }

    selectAddress(addressId) {
        this.props.selectAddress(addressId);
    }

    saveAddress(address) {
        this.props.saveAddress(address);
    }

    placeOrder() {
        this.props.placeOrder(this.props.checkout);
    }

    logout(redirectUrl) {
        this.props.logout(redirectUrl);
    }

    getCheckoutComponents() {
        return (
            <CheckoutComponents
                checkout={this.props.checkout}
                handleSelectAddress={this.selectAddress.bind(this)}
                handleSaveAddress={this.saveAddress.bind(this)}
                placeOrder={this.placeOrder.bind(this)}
                logout={this.logout.bind(this)}
            />
        )
    }

    getCartPriceDetail() {
        var style={
            backgroundColor: "#2094ae",
            color: "#fff"
        }

        return (
            <CartPriceDetail
                amountPayable={this.props.checkout.orderSummary.amountPayable}
                style={style}
                />
        )
    }

    render() {
        if(this.props.checkout) {
            if(this.props.checkout.error) {
                return (
                    <InternalServerError />
                )
            }

            if(this.props.checkout.orderSummary.cartItems.length == 0) {
                return (
                    <Redirect to="/cart" />
                )
            }

            return (
                <div className="pure-g checkout-container">
                    <div className="pure-u-md-17-24">
                        {this.getCheckoutComponents()}
                    </div>
                    <div className="pure-u-md-7-24">
                        {this.getCartPriceDetail()}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="pure-g checkout-container">
                    <LoadingSpinner />
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        checkout: state.checkout
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ checkoutInit, selectAddress, saveAddress, placeOrder, logout }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Checkout);