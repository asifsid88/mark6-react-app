import React from 'react';
import {Component} from 'react';
import Slideshow from './slideshow';
import DealBanner from './deal-banner';

class HomePage extends Component {

    render() {
        return (
            <div>
                <Slideshow />
                <DealBanner type="Top Offer" />
                <DealBanner type="Newly Launched" />
            </div>
        )
    }
}

export default HomePage;