import React from 'react';
import {Component} from 'react';

class OrderTotalPanel extends Component {

    render() {
        return (
            <div className="total-price-panel">
                <div className="pure-u-md-1-2 align-left">
                    <span className="order-label">Ordered On </span> {this.props.orderDate}
                </div>
                <div className="pure-u-md-1-2 align-right font-size-16">
                    <span className="text-bold order-label">Order Total </span>
                    <span className="text-bold">{String.fromCharCode(parseInt('0x20B9', 16))} {this.props.amount.totalAmountPayable}</span>

                    <span className="text-bold order-label padding-left color-green">Total Saving </span>
                    <span className="text-bold color-green">{String.fromCharCode(parseInt('0x20B9', 16))} {this.props.amount.totalSaving}</span>
                </div>
            </div>
        )
    }
}

export default OrderTotalPanel;