import React from 'react';
import {Component} from 'react';
import {getAddressString} from '../utility';

class OrderSummary extends Component {

    getOrderDetailHeader() {
        return (
            <div className="pure-u-md-1 order-detail-wrapper">
                <div className="pure-u-md-1-3 order-detail-heading">
                    Order Details
                </div>
                <div className="pure-u-md-5-12 order-detail-heading center-right-module">
                    Address
                </div>
                <div className="pure-u-md-1-4 order-detail-heading center-right-module">
                    Manage Order
                </div>
            </div>
        )
    }

    getOrderDetailContent() {
        return (
            <div className="pure-u-md-1 order-detail-wrapper">
                <div className="pure-u-md-1-3 order-detail-content order-detail-content-left-module">
                    {this.getOrderContent()}
                </div>
                <div className="pure-u-md-5-12 order-detail-content center-right-module">
                    {this.getAddressContent()}
                </div>
                <div className="pure-u-md-1-4 order-detail-content center-right-module">
                    {this.getManageOrderContent()}
                </div>
            </div>
        )
    }

    getOrderContent() {
        const itemString = this.props.order.cartItems.length == 1 ? "item" : "items";

        return (
            <div className="pure-u-md-1">
                <div className="content-row">
                    <div className="pure-u-md-1-4 content-row-title">
                        <span className="content-row-title-data">Order ID</span>
                    </div>
                    <div className="pure-u-md-3-4 content-row-desc">
                        <div>{this.props.order.id}
                            <span className="content-row-order-item-count">
                                ({this.props.order.cartItems.length} {itemString})
                            </span>
                        </div>
                    </div>
                </div>

                <div className="content-row">
                    <div className="pure-u-md-1-4 content-row-title">
                        <span className="content-row-title-data">Order Date</span>
                    </div>
                    <div className="pure-u-md-3-4 content-row-desc">
                        <div>{this.props.order.orderCreationDate}</div>
                    </div>
                </div>

                <div className="content-row">
                    <div className="pure-u-md-1-4 content-row-title">
                        <span className="content-row-title-data">Total Amount</span>
                    </div>
                    <div className="pure-u-md-3-4 content-row-desc">
                        <div>{String.fromCharCode(parseInt('0x20B9', 16))} {this.props.order.amountPayable.totalAmountPayable} through Cash on delivery</div>
                    </div>
                </div>
            </div>
        )
    }

    getAddressContent() {
        return (
            <div className="pure-u-md-1">
                <div className="text-bold">{this.props.order.deliveryAddress.contactPerson} </div>
                <div className="padding-top">{getAddressString(this.props.order.deliveryAddress)}</div>
                <div className="padding-top">
                    <span className="text-bold">Phone</span>
                    <span className="padding-left">{this.props.order.deliveryAddress.phone}</span>
                </div>
            </div>
        )
    }

    getManageOrderContent() {
        return (
            <div className="pure-u-md-1">
                <div>
                    <div className="manage-order-desc">
                        <span className="">
                            <div className="manage-order-wrapper">
                                <svg fill="#2094ae" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" className="manage-order-icon">
                                    <path d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z"></path>
                                </svg>
                                <span className="manage-order-text">Request Invoice</span>
                            </div>
                        </span>
                    </div>
                </div>

                <div className="manage-order-wrapper">
                    <img src="/static/img/need-help-icon.svg" className="manage-order-icon" />
                    <span className="manage-order-text">Need Help?</span>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="order-container">
                {this.getOrderDetailHeader()}
                {this.getOrderDetailContent()}
            </div>
        )
    }
}

export default OrderSummary;