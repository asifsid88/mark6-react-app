import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import AccountPanel from './account-panel';
import AccountSubscriptionDetail from './account-subscription-detail';
import LoadingSpinner from './loading-spinner';
import {getCustomerDetail} from '../actions/action-get-customer-detail';

class Subscriptions extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCustomerDetail();
    }

    getAccountPanel() {
        if(this.props.customer) {
            return (
                <AccountPanel
                    name={this.props.customer.name}
                    pictureUrl={this.props.customer.pictureUrl}
                    tab="subscriptions"
                />
            )
        } else {
            <LoadingSpinner />
        }
    }

    getAccountSubscriptionDetail() {
        return (
            <AccountSubscriptionDetail />
        )
    }

    render() {
        return (
            <div className="pure-g account-container">
                <div className="pure-u-md-7-24">
                    {this.getAccountPanel()}
                </div>

                <div className="pure-u-md-17-24 account-detail-container-wrapper">
                    {this.getAccountSubscriptionDetail()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        customer: state.customer
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCustomerDetail }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Subscriptions);