import React from 'react';
import {Component} from 'react';

class CartPriceDetail extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const pieceString = this.props.amountPayable.totalItemCount <= 1 ? " piece" : " pieces";

        return (
            <div className="cart-price-detail-container">
                <div className="cart-price-detail-wrapper">
                    <div className="cart-price-detail-heading" style={this.props.style}>
                        <span>Price details</span>
                    </div>
                    <div className="cart-price-detail-summary">
                        <div className="cart-price-detail-total">
                            <div>Price ({this.props.amountPayable.totalItemCount} {pieceString})</div>
                            <div className="">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.amountPayable.totalCost}</div>
                        </div>
                        <div className="cart-price-detail-delivery">
                            <div>Delivery Charges</div>
                            <div className="">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.amountPayable.deliveryCharge}</div>
                        </div>
                        <div className="cart-price-detail-total-pay-wrapper">
                            <div className="cart-price-detail-total">
                                <div>Amount Payable</div>
                                <div className="">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.amountPayable.totalAmountPayable}</div>
                            </div>
                        </div>
                    </div>
                    <div className="cart-price-detail-contact">
                        <span className="cart-price-detail-contact-text">
                            You may anytime reach us at +91 9004 522 900
                        </span>
                    </div>
                </div>

                <div className="cart-price-detail-secure">
                    <img src="/static/img/secure.png" className="cart-price-detail-secure-image" />
                    <span className="cart-price-detail-return-policy-text">Safe and Secure Payments. Easy returns. 100% Authentic products. End-to-end manufactured by us.</span>
                </div>
            </div>

        )
    }
}

export default CartPriceDetail;
