import React from 'react';
import {Component} from 'react';

class ImageThumbnail extends Component {

    render() {
        return (
            <div className="image-thumbnail" onClick={() => this.props.selectImage(this.props.imageUrl)}>
                <img className="pure-img" src={this.props.imageUrl} />
            </div>
        )
    }
}

export default ImageThumbnail;