import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';
import UserProfileMenuBar from './user-profile-menu-bar';

class TopLinkBar extends Component {

    render() {
        return (
            <div className="top-link-bar-wrapper pure-g">
                <div className="pure-u-1 vertical-center align-right">
                    <div className="pure-menu pure-menu-horizontal">
                        <ul className="pure-menu-list">
                            <li className="pure-menu-item"><Link to='/' className="pure-menu-link">Home</Link></li>
                            <li className="pure-menu-item"><Link to='/catalog' className="pure-menu-link">Catalog</Link></li>
                            <li className="pure-menu-item"><Link to='/about-us' className="pure-menu-link">About Us</Link></li>
                            <li className="pure-menu-item"><Link to='/contact-us' className="pure-menu-link">Contact Us</Link></li>
                            <UserProfileMenuBar />
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default TopLinkBar;