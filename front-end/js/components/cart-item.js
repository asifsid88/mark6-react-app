import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {createProductPageUrl} from '../utility';
import {bindActionCreators} from 'redux';
import {removeCartItem} from '../actions/action-remove-cart-item';
import {updateQuantity} from '../actions/action-update-quantity';

class CartItem extends Component {

    constructor(props) {
        super(props);
    }

    getDecreaseQtyButton() {
        if(this.props.cartItem.quantity <= 1) {
            return (
                <button className="cart-action-qty-change-btn" disabled onClick={() => this.props.updateQuantity(this.props.cartItem.m6sin, this.props.cartItem.quantity - 1, this.props.cartRefId)}>-</button>
            )
        } else {
            return (
                <button className="cart-action-qty-change-btn" onClick={() => this.props.updateQuantity(this.props.cartItem.m6sin, this.props.cartItem.quantity - 1, this.props.cartRefId)}>-</button>
            )
        }
    }

    getIncreaseQtyButton() {
        if(this.props.cartItem.quantity >= 10) {
            return (
                <button className="cart-action-qty-change-btn" disabled onClick={() => this.props.updateQuantity(this.props.cartItem.m6sin, this.props.cartItem.quantity + 1, this.props.cartRefId)}>+</button>
            )
        } else {
            return (
                <button className="cart-action-qty-change-btn" onClick={() => this.props.updateQuantity(this.props.cartItem.m6sin, this.props.cartItem.quantity + 1, this.props.cartRefId)}>+</button>
            )
        }
    }

    render() {
        const productUrl = createProductPageUrl(this.props.cartItem.productId,
                                                this.props.cartItem.styleCode,
                                                this.props.cartItem.title);
        return (
            <div className="cart-item-container">
                <div className="cart-product-panel">
                    <div className="cart-product-image">
                        <Link to={productUrl}>
                            <img className="pure-img" src={this.props.cartItem.imageUrl} />
                        </Link>
                    </div>
                    <div className="cart-product-details">
                        <div className="cart-product-title">
                            <Link to={productUrl} className="cart-product-title-link">
                                {this.props.cartItem.title}
                            </Link>
                        </div>
                        <div className="cart-product-color-size">{this.props.cartItem.color}, {this.props.cartItem.size}</div>
                        <div className="cart-product-style-code">{this.props.cartItem.styleCode}</div>
                        <div className="cart-product-price">
                            <div className="cart-product-price-sp">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.cartItem.price.sellingPrice}</div>
                            <div className="cart-product-price-mrp">{String.fromCharCode(parseInt('0x20B9', 16))}{this.props.cartItem.price.mrp}</div>
                            <div className="cart-product-price-offer">{this.props.cartItem.price.offer}% Off</div>
                        </div>
                    </div>
                    <div className="cart-product-sla">Free delivery by Wed, Oct 4</div>
                </div>
                <div className="cart-action-panel">
                    <div className="cart-action">
                        <div className="cart-action-wrapper">
                            {this.getDecreaseQtyButton()}
                            <div className="cart-action-qty-input">
                                <input className="cart-action-qty-input-field" type="text" value={this.props.cartItem.quantity} readOnly />
                            </div>
                            {this.getIncreaseQtyButton()}
                        </div>
                    </div>
                    <div className="cart-action-item-remove" onClick={() => this.props.removeCartItem(this.props.cartItem.m6sin, this.props.cartRefId)}>
                        <div className="cart-action-item-remove-text">Remove</div>
                    </div>
                </div>
            </div>
        )
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ removeCartItem, updateQuantity }, dispatch);
}

export default connect(null, matchDispatchToProps)(CartItem);
