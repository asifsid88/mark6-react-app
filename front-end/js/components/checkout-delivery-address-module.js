import React from 'react';
import {Component} from 'react';
import {isStringNullOrEmpty, getAddressString} from '../utility';

class DeliveryAddress extends Component {

    constructor(props) {
        super(props);
    }

    getEditButton() {
        if(this.props.isSelected) {
            return (
                <div className="checkout-delivery-address-edit-wrapper">
                    <button type="button"
                            className="checkout-delivery-address-edit-button"
                            onClick={(event) => {
                                        event.stopPropagation();
                                        this.props.clickEditAddress(this.props.address.id);
                                        }
                                     }>EDIT</button>
                </div>
            )
        }
    }

    getDeliverHereButton() {
        if(this.props.isSelected) {
            return (
                <button className="checkout-delivery-address-button"
                        onClick={() => this.props.confirmComponent(this.props.index, this.props.address.id)}>Deliver Here</button>
            )
        }
    }

    render() {
        let deliveryAddressCssClass = "checkout-delivery-address";
        if(this.props.isSelected) {
            deliveryAddressCssClass += " checkout-delivery-address-selected";
        }

        return(
            <div className={deliveryAddressCssClass} onClick={() => this.props.onPickAddress(this.props.address.id)}>
                <input type="radio"
                       className="checkout-delivery-address-radio"
                       name="address"
                       checked={this.props.isSelected}
                       id={this.props.address.id}
                       readOnly />
                <div className="checkout-delivery-address-wrapper">
                    <div className="checkout-delivery-address-container">
                        <div className="checkout-delivery-address-detail">
                            <p className="checkout-delivery-address-detail-wrapper">
                                <span className="checkout-delivery-address-data">{this.props.address.contactPerson}</span>
                                <span className="checkout-delivery-address-data checkout-delivery-address-padding">{this.props.address.phone}</span>
                            </p>
                            <span className="checkout-delivery-address-complete">
                                {getAddressString(this.props.address)}
                            </span>
                            {this.getDeliverHereButton()}
                        </div>
                        {this.getEditButton()}
                    </div>
                </div>
            </div>
        )
    }
}

export default DeliveryAddress;