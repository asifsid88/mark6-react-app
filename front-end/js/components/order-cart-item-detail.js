import React from 'react';
import {Component} from 'react';
import {createProductPageUrl} from '../utility';

class OrderCartItemDetail extends Component {

    getColumn1() {
        const productUrl = createProductPageUrl(this.props.cartItem.productId,
                                                this.props.cartItem.styleCode,
                                                this.props.cartItem.title);
        return (
            <div className="pure-u-md-1-2">
                <div>
                    <div className="pure-u-md-1-4 column-1-img-wrapper">
                        <a href={`${productUrl}`} target="_blank">
                            <div className="column-1-img-container">
                                <img className="pure-img" src={this.props.cartItem.imageUrl} alt={this.props.cartItem.title} title={this.props.cartItem.title} />
                            </div>
                        </a>
                    </div>
                    <div className="pure-u-md-3-4 column-1-data-data-wrapper">
                        <a className="column-1-data-title" href={`${productUrl}`} target="_blank">{this.props.cartItem.title}</a>
                        <div className="column-1-data-wrapper">
                            <span className="column-1-data-label">Color: </span>
                            <span className="column-1-data-value">{this.props.cartItem.color}</span>
                        </div>
                        <div className="column-1-data-wrapper">
                            <span className="column-1-data-label">Size: </span>
                            <span className="column-1-data-value">{this.props.cartItem.size}</span>
                        </div>
                        <div className="column-1-data-wrapper">
                            <span className="column-1-data-label">Style Code: </span>
                            <span className="column-1-data-value">{this.props.cartItem.styleCode}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    getColumn2() {
        const totalPrice = parseInt(this.props.cartItem.price.sellingPrice) * parseInt(this.props.cartItem.quantity)
        return (
            <div className="pure-u-md-1-4 column-2">{String.fromCharCode(parseInt('0x20B9', 16))} {this.props.cartItem.price.sellingPrice} x {this.props.cartItem.quantity}<br />
                <div className="text-bold">{String.fromCharCode(parseInt('0x20B9', 16))} {totalPrice}</div>
                <div className="column-2-wrapper">
                    <span className="column-2-title">OFFERS: </span>1
                </div>
            </div>
        )
    }

    getColumn3() {
        return (
            <div className="pure-u-md-1-4 column-3">Ordered on {this.props.orderDate}
                <div className="column-3-title">Your item has been ordered</div>
            </div>
        )
    }

    render() {
        return (
            <div className="order-cart-item-wrapper">
                <div className="order-cart-item-container">
                    {this.getColumn1()}
                    {this.getColumn2()}
                    {this.getColumn3()}
                </div>
            </div>
        )
    }
}

export default OrderCartItemDetail;