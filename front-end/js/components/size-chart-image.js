import React from 'react';
import {Component} from 'react';

class SizeChartImage extends Component {

    render() {
        return (
            <img className="pure-img size-chart-image" src="/static/img/size-chart.jpg" />
        )
    }
}

export default SizeChartImage;