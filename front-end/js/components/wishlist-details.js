import React from 'react';
import {Component} from 'react';
import WishlistItem from './wishlist-item';
import CartButtonPanel from './cart-button-panel';
import LoadingSpinner from './loading-spinner';

class WishlistDetails extends Component {

    constructor(props) {
        super(props);
    }

    getEmptyCartText() {
        return (
            <div className="cart-item-container">
                There is no Item in your wishlist.
            </div>
        )
    }

    getCartItems() {
        if(this.props.wishlist) {
            if(this.props.wishlist.length == 0) {
                return (
                    <div>{this.getEmptyCartText()}</div>
                )
            }

            return (
                this.props.wishlist.map((cartItem) => {
                    return (
                        <WishlistItem
                            key={cartItem.productId}
                            cartItem={cartItem}
                            removeWishlistItem={this.props.removeWishlistItem}
                         />
                    )
                })
            )
        } else {
            return (
                <div>{this.getEmptyCartText()}</div>
            )
        }
    }

    getCartButtonPanel() {
        if(!this.props.wishlist || this.props.wishlist.length == 0) {
            return (
                <CartButtonPanel cartItemCount={0} />
            )
        }
    }

    render() {
        const cartItemCount = this.props.wishlist.length;

        var styleContainer = {};
        if(cartItemCount == 0) {
            styleContainer = {
                marginRight: 0
            }
        }

        return (
            <div className="cart-detail-container" style={styleContainer}>
               <div className="cart-heading">My Wishlist ({cartItemCount})</div>
               <div>{this.getCartItems()}</div>
               <div>{this.getCartButtonPanel()}</div>
            </div>
        )
    }
}

export default WishlistDetails;