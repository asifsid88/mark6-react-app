import React from 'react';
import {Component} from 'react';

class CheckoutCompletedComponent extends Component {

    constructor(props) {
        super(props);
    }

    getContent() {
        if(this.props.title) {
            return (
                <div className="checkout-complete-component-detail-wrapper">
                    <div>
                        <span className="text-bold">{this.props.title}</span>
                        <span className="checkout-complete-component-detail-content">{this.props.content}</span>
                    </div>
                </div>
            )
        }
    }

    getTickedSvg() {
        if(this.props.title) {
            return (
                <svg height="10" width="16" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="checkout-complete-component-heading-selected-svg">
                    <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" stroke="#2094ae">
                    </path>
                </svg>
            )
        }
    }

    getChangeButton() {
        if(this.props.title) {
            return (
                <button className="checkout-component-change-btn" onClick={() => this.props.editComponent(this.props.index)}>Change</button>
            )
        }
    }

    render() {
        return(
            <div className="checkout-component-container">
                <div className="checkout-component-wrapper">
                    <div className="checkout-component-cover">
                        <h3 className="checkout-component-heading-index checkout-component-heading-index-selected">
                            <span className="checkout-component-heading-index-span">{this.props.index}</span>
                        </h3>
                        <div className="checkout-complete-component-heading-wrapper">
                            <div className="checkout-complete-component-heading" style={this.props.style}>{this.props.heading}
                                {this.getTickedSvg()}
                            </div>
                            {this.getContent()}
                        </div>
                        {this.getChangeButton()}
                    </div>
                </div>
            </div>
        )
    }
}

export default CheckoutCompletedComponent;