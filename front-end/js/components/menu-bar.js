import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getCart} from '../actions/action-get-cart';
import {isUserLoggedIn} from '../actions/action-user-logged-in';

class MenuBar extends Component {

    componentDidMount() {
        this.props.isUserLoggedIn();
        this.props.getCart();
    }

    getCartCount() {
        if(!this.props.cart) {
            return 0;
        } else {
            return this.props.cart.cartItems.length;
        }
    }

    render() {
        return (
            <div className="menu-wrapper pure-g">
                <div className="pure-u-md-1-8 vertical-center">
                    <a href="/"><img src="/static/img/logo.png" className="vertical-center" /></a>
                </div>
                <div className="pure-u-md-7-8">
                    <div className="pure-u-md-7-8">
                        <input type="text" className="search-bar" placeholder="Search for Products" />
                    </div>
                    <div className="pure-u-md-1-8">
                        <Link to="/cart">
                            <div className="cart-button">
                                <svg className="cart-icon" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                                    fill="#fff"></path>
                                </svg>
                                Cart
                                <span className="cart-item-count">{this.getCartCount()}</span>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        cart: state.cart,
        userLoggedIn: state.userLoggedIn
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getCart, isUserLoggedIn }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(MenuBar);