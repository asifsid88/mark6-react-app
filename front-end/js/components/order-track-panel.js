import React from 'react';
import {Component} from 'react';
import {createOrderDetailPageUrl} from '../utility';

class OrderTrackPanel extends Component {
    render() {
        const orderDetailPageUrl = createOrderDetailPageUrl(this.props.orderId);

        return (
            <div className="track-button-panel">
                <div className="pure-u-md-3-4">
                    <div className="button-container">
                        <a href={`${orderDetailPageUrl}`} target="_blank">
                            <div className="order-label-button">{this.props.orderId}</div>
                        </a>
                    </div>
                </div>
                <div className="pure-u-md-1-4 align-right">
                    <div className="track-button-container">
                        <a href={`${orderDetailPageUrl}`} target="_blank">
                            <div className="track-button-wrapper">
                                <svg width="12" height="12" viewBox="0 0 9 12" className="track-svg" xmlns="http://www.w3.org/2000/svg">
                                    <path fill="#2094ae" d="M4.2 5.7c-.828 0-1.5-.672-1.5-1.5 0-.398.158-.78.44-1.06.28-.282.662-.44 1.06-.44.828 0 1.5.672 1.5 1.5 0 .398-.158.78-.44 1.06-.28.282-.662.44-1.06.44zm0-5.7C1.88 0 0 1.88 0 4.2 0 7.35 4.2 12 4.2 12s4.2-4.65 4.2-7.8C8.4 1.88 6.52 0 4.2 0z" fillRule="evenodd"></path>
                                </svg>
                                <span>Track</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default OrderTrackPanel;