import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingSpinner from './loading-spinner';
import OrderCartItems from './order-cart-items';
import AccountPanel from './account-panel';
import NoOrderFound from './no-order-found';
import {getOrders} from '../actions/action-get-orders';
import {getCustomerDetail} from '../actions/action-get-customer-detail';

class Orders extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getCustomerDetail();
        this.props.getOrders();
    }

    getAccountPanel() {
        if(this.props.customer) {
            return (
                <AccountPanel
                    name={this.props.customer.name}
                    pictureUrl={this.props.customer.pictureUrl}
                    tab="orders"
                />
            )
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    getOrderCartItems() {
        if(this.props.orderList) {
            if(this.props.orderList.length == 0) {
                return (
                    <NoOrderFound
                        msg="You don't have any Order. Shop now to create your First Order."
                    />
                )
            } else {
                return (
                    this.props.orderList.map((order) => {
                        return (
                            <OrderCartItems
                                showTrackPanel={true}
                                order={order}
                                key={order.id}
                            />
                        )
                    })
                )
            }
        } else {
            return (
                <LoadingSpinner />
            )
        }
    }

    render() {
        return (
            <div className="pure-g order-detail-container">
                <div className="pure-u-md-7-24">
                    {this.getAccountPanel()}
                </div>

                <div className="pure-u-md-17-24 account-detail-container-wrapper">
                    {this.getOrderCartItems()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        orderList: state.orderList,
        customer: state.customer
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getOrders, getCustomerDetail }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Orders);