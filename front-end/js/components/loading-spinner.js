import React from 'react';
import {Component} from 'react';

var centerAlignStyle = {
    display: 'block',
    margin: 'auto'
}

class LoadingSpinner extends Component {

    render() {
        return (
            <div className="pure-u-md-1 card vertical-center">
                <img src="/static/img/loading-spinner.gif" style={centerAlignStyle}  />
            </div>
        )
    }
}

export default LoadingSpinner;