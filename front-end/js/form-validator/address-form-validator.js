export function validateForm(field) {
    switch(field.type) {
        case 'text':
        case 'textarea':
            return validateTextField(field);
        default:
            return false;
    }
}

function validateTextField(field) {
    switch(field.name) {
        case 'contactPerson':
        case 'name':
        case 'state':
        case 'city':
            return validateNameField(field.value);

        case 'street1':
        case 'landmark':
            return validateAddressField(field.value);

        case 'street2':
            return validateOptionalField(field.value) || validateAddressField(field.value);

        case 'phone':
            return validatePhoneField(field.value);

        case 'alternatePhone':
            return validateOptionalField(field.value) || validatePhoneField(field.value);

        case 'pincode':
            return validatePincodeField(field.value);

        case 'email':
            return validateEmailAddressField(field.value);

        default:
            return false;
    }
}

function validateNameField(value) {
    var regEx = /^[a-z0-9 ]{2,}$/i;
    return regEx.test(value);
}

function validatePhoneField(value) {
    var regEx = /^[0-9]{10}$/;
    return regEx.test(value);
}

function validatePincodeField(value) {
    var regEx = /^[0-9]{6}$/;
    return regEx.test(value);
}

function validateAddressField(value) {
    return value!==null && value!=="" && value.length>3;
}

function validateOptionalField(value) {
    return value==null || value.length==0;
}

function validateEmailAddressField(value) {
    var regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regEx.test(value);
}