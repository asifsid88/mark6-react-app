import {docCookies} from './cookie';

export const cookies = docCookies;

export function getApiURL(apiUri) {
    return window.location.protocol + "//" +
            window.location.hostname + ":" +
            window.location.port + "/api/" +
            apiUri;
}

export function isStringNullOrEmpty(text) {
    return (text === undefined || text === null || text === '')
}

export function createProductPageUrl(productId, styleCode, title) {
    let productName = `${title}`;
    productName = productName.replace(/\W+/g, '-').toLowerCase();

    return (`/${productName}/p/${styleCode}?pid=${productId}`);
}

export function createOrderDetailPageUrl(orderId) {
    return (`/order_details?orderId=${orderId}`);
}

export function storageAvailable() {
    try {
        var storage = window['localStorage'],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch(e) {
        return false;
    }
}

export function isUserLoggedIn() {
    return !isStringNullOrEmpty(docCookies.getItem('m6NSid'));
}

export function base64Encode(str) {
   return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
       function toSolidBytes(match, p1) {
           return String.fromCharCode('0x' + p1);
   }));
}

export function base64Decode(str) {
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

export function deepClone(obj) {
    var clone, nestedObj, key;
    clone = Array.isArray(obj) ? [] : {};

    for(key in obj) {
        nestedObj = obj[key];
        clone[key] = (typeof v === "object") ? deepClone(nestedObj) : nestedObj;
    }

    return clone;
}

export function getAddressString(address) {
    let addressStr = address.street1;
    if(!isStringNullOrEmpty(address.street1)) {
        addressStr += ", ";
    }

    addressStr += address.street2
                + ", "
                + address.landmark
                + ", "
                + address.city
                + ", "
                + address.state
                + ", "
                + address.country
                + " - "
                + address.pincode;

    return addressStr;
}

export function getCurrentUrl() {
    return encodeURIComponent(window.location.href);
}