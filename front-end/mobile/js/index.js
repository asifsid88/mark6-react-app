import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger'
import reduxMulti from 'redux-multi';
import { batchedSubscribe } from 'redux-batched-subscribe';
import allReducers from '../../js/reducers';
import App from './components/app';

const loggerMiddleware = createLogger();

const store = createStore(
                allReducers,
                applyMiddleware(
                    thunkMiddleware,
                    reduxMulti,
                    loggerMiddleware
                )
              );

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
  </Provider>, document.getElementById('root'));
