import React from 'react';
import {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="card footer-wrapper">
                <div className="follow float-right">
                    <div className="keep-in-touch">Keep in touch</div>
                    <a href="https://www.facebook.com/mark6clothing" target="_blank"><div className="social facebook"></div></a>
                    <a href="https://www.instagram.com/mark6clothing/" target="_blank"><div className="social instagram"></div></a>
                    <a href="https://twitter.com/mark6clothing" target="_blank"><div className="social twitter"></div></a>
                </div>
                <div className="clear-both"></div>
                <div>
                    <div className="float-left"><a href="/contact-us" className="footer-link">Need Help?</a></div>
                    <div className="float-right">&copy; {new Date().getFullYear()} <a href="/" className="footer-link">www.mark6.in</a>. All rights reserved.</div>
                </div>
            </div>
        )
    }
}

export default Footer;
