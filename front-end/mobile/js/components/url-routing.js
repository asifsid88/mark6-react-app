import React from 'react';
import {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import NoPageFound from './no-page-found';

class URLRouting extends Component {
    render() {
        return(
            <div className="container-wrapper">
                <Switch>
                    <Route component={NoPageFound} />
                </Switch>
            </div>
        )
    }
}

export default URLRouting;