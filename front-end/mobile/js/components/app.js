import React from 'react';
import {Component} from 'react';
import Header from './header';
import URLRouting from './url-routing';
import Footer from './footer';

class App extends Component {
  render() {
    return(
      <div className="main-container">
        <Header />
        <div className="body-wrapper">
            <URLRouting />
            <Footer />
        </div>
      </div>
    );
  }
}

export default App;
