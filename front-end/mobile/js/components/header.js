import React from 'react';
import {Component} from 'react';
import TopLinkBar from './top-link-bar';

class Header extends Component {
    render() {
        return (
            <div className="header-wrapper">
                <TopLinkBar />
            </div>
        )
    }
}

export default Header;
