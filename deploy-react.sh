#!/bin/bash
echo 'Webpack: Building and packaging Mark6 ReactApp'
webpack


echo 'Cleaning TCP port:3000'
touch temp-react.txt
lsof -i tcp:3000 > temp-react.txt

file="temp-react.txt"
while IFS= read line
do
    processName=$(echo $line | awk '{print $1}')

    if [[ $processName == 'node' ]]
    then
        nodeProcessId=$(echo $line | awk '{print $2}')
        echo 'React App running with PID='$nodeProcessId
        kill -9 $nodeProcessId
        echo 'React App running at 3000 is stopped.'
        break
    fi
done <"$file"


echo 'Run Mark6 React App'
npm start

echo 'Mark6 React App running on: http://localhost:3000/'
