var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const Dotenv = require('dotenv-webpack');

module.exports = env => {
    const _path = './.env.' + (env && env.NODE_ENV || 'dev');

    return {
        devServer: {
            inline: true,
            contentBase: './src/main/webapp/',
            port: 3000
        },
        entry: {
            index: './front-end/js/index.js',
            styles: './front-end/css/index.js',
            mindex: './front-end/mobile/js/index.js',
            mstyles: './front-end/mobile/css/index.js'
        },
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    loaders: ['babel-loader'],
                    exclude: /node_modules/
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: "css-loader"
                    })
                },
                {
                    test: /\.png$/,
                    loader: "file-loader"
                }
            ]
        },
        output: {
            path: path.resolve(__dirname, 'src/main/webapp/static/'),
            filename: 'js/[name].min.js',
            publicPath: '/static/'
        },
        plugins: [
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin(),
            new webpack.optimize.AggressiveMergingPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new HtmlWebpackPlugin({
                hash: true,
                title: 'Mark6',
                filename: '../index.html',
                template: 'index.template.ejs',
                inject: 'body',
                chunks: ['index', 'styles'],
                excludeAssets: [/styles.*.js/]
            }),
            new HtmlWebpackPlugin({
                hash: true,
                title: 'Mark6',
                filename: '../mindex.html',
                template: 'index.template.ejs',
                inject: 'body',
                chunks: ['mindex', 'mstyles'],
                excludeAssets: [/styles.*.js/]
            }),
            new HtmlWebpackExcludeAssetsPlugin(),
            new ExtractTextPlugin({
                filename: "css/[name].bundle.css"
            }),
            new Dotenv({
                path: _path
            })
        ]
    }
}
