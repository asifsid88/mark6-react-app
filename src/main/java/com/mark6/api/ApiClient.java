package com.mark6.api;

import com.google.gson.Gson;
import com.mark6.constants.PropertyConfigurationConstants;
import com.mark6.model.Response;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mhussaa on 9/11/17.
 */
@Component
@Log4j2
public class ApiClient {

    private static final String BASIC_AUTH_USERNAME = "mark6-api";
    private static final String BASIC_AUTH_PASSWORD = "mark6";

    private static final String HEADER_ACCEPT = "accept";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_APPLICATION_JSON = "application/json";

    private static final String INTERNAL_SERVER_ERROR = "Internal server error occurred.";

    @Autowired
    private Environment environment;

    @Autowired
    public ApiClient() {
        setUnirestObjectMapper();
    }

    public Response getJSON(String url, Map<String, String> pathParams, Map<String, Object> queryStrings) {
        Response response;
        try {
            HttpRequest getRequest = Unirest.get(environment.getProperty(PropertyConfigurationConstants.API_BASE_URL) + url)
                    .basicAuth(BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD)
                    .headers(getHeaders())
                    .queryString(queryStrings);
            addRouteParam(getRequest, pathParams);

            HttpResponse<JsonNode> apiResponse = getRequest.asJson();
            if(apiResponse.getStatus() == 200) {
                response = new Gson().fromJson(apiResponse.getBody().toString(), Response.class);
            } else {
                response = createErrorResponse();
            }
        } catch (UnirestException e) {
            log.error("Error while making get api call: {}, Error: {}", url, e);
            response = createErrorResponse();
        }

        return response;
    }

    public Response postJSON(String url, Object payload) {
        Response response;
        try {
            HttpResponse<JsonNode> apiResponse = Unirest.post(environment.getProperty(PropertyConfigurationConstants.API_BASE_URL) + url)
                                .headers(getHeaders())
                                .body(payload)
                                .asJson();

            if(apiResponse.getStatus() == 200) {
                response = new Gson().fromJson(apiResponse.getBody().toString(), Response.class);
            } else {
                response = createErrorResponse();
            }
        } catch(UnirestException e) {
            log.error("Error while making post api call: {}, Payload: {}, Error: {}", url, payload, e);
            response = createErrorResponse();
        }

        return response;
    }

    private void setUnirestObjectMapper() {
        Unirest.setObjectMapper(new ObjectMapper() {

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return new Gson().fromJson(value, valueType);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public String writeValue(Object value) {
                try {
                    return new Gson().toJson(value);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void addRouteParam(HttpRequest httpRequest, Map<String, String> pathParams) {
        if(pathParams == null) return;

        for(Map.Entry<String, String> pathParam : pathParams.entrySet()) {
            httpRequest.routeParam(pathParam.getKey(), pathParam.getValue());
        }
    }

    private Map<String,String> getHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(HEADER_ACCEPT, HEADER_APPLICATION_JSON);
        headers.put(HEADER_CONTENT_TYPE, HEADER_APPLICATION_JSON);

        return headers;
    }

    private Response createErrorResponse() {
        Response response = new Response();
        response.setStatus(500);
        response.setMessage(INTERNAL_SERVER_ERROR);

        return response;
    }
}
