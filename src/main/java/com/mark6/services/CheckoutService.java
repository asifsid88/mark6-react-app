package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.Util;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CheckoutService {

    private ApiClient apiClient;

    @Autowired
    public CheckoutService(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public Response init(String customerId, String productId) {
        Map<String, Object> queryParam = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId)
        );

        return apiClient.getJSON(ApiMapping.CHECKOUT_INIT, null, queryParam);
    }

    public Response selectAddress(String customerId, String addressId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_ADDRESS_ID, addressId)
        );

        return apiClient.getJSON(ApiMapping.SELECT_ADDRESS, null, queryString);
    }

    public Response placeOrder(Object payload) {
        return apiClient.postJSON(ApiMapping.PLACE_ORDER, payload);
    }
}
