package com.mark6.services;

import com.mark6.model.Customer;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ServiceFacade {

    private ProductService productService;
    private CatalogService catalogService;
    private CustomerService customerService;
    private CartService cartService;
    private CheckoutService checkoutService;
    private OrderService orderService;
    private WishlistService wishlistService;

    @Autowired
    public ServiceFacade(ProductService productService, CatalogService catalogService,
                         CustomerService customerService, CartService cartService,
                         CheckoutService checkoutService, OrderService orderService,
                         WishlistService wishlistService) {
        this.productService = productService;
        this.catalogService = catalogService;
        this.customerService = customerService;
        this.cartService = cartService;
        this.checkoutService = checkoutService;
        this.orderService = orderService;
        this.wishlistService = wishlistService;
    }

    public Response getProductById(String productId) {
        return productService.getProductById(productId);
    }

    public Response getCatalog(String catalogId) {
        return catalogService.getCatalog(catalogId);
    }

    public Customer getCustomer(Customer customer) {
        return customerService.getCustomer(customer);
    }

    public String getAccessToken(String customerId) {
        return customerService.getAccessToken(customerId);
    }

    public boolean logout(String customerId) {
        return customerService.logout(customerId);
    }

    public Response addToCart(String customerId, String productId, int quantity) {
        return cartService.addToCart(customerId, productId, quantity);
    }

    public Response getCart(String customerId) {
        return cartService.getCart(customerId);
    }

    public Response removeCartItem(String cartRefId, String customerId, String cartItemId) {
        return cartService.removeCartItem(cartRefId, customerId, cartItemId);
    }

    public Response updateQuantity(String cartRefId, String customerId, String cartItemId, String quantity) {
        return cartService.updateQuantity(cartRefId, customerId, cartItemId, quantity);
    }

    public Response getCustomerDetail(String customerId) {
        return customerService.getCustomerDetail(customerId);
    }

    public Response checkoutInit(String customerId, String productId) {
        return checkoutService.init(customerId, productId);
    }

    public Response getAddressList(String customerId) {
        return customerService.getAddressList(customerId);
    }

    public Response selectAddress(String customerId, String addressId) {
        return checkoutService.selectAddress(customerId, addressId);
    }

    public Response saveAddress(String customerId, Map<String, Object> saveAddressFormData) {
        return customerService.saveAddress(customerId, saveAddressFormData);
    }

    public Response updateCustomerDetail(String customerId, Map<String, Object> customerFormData) {
        return customerService.updateCustomerDetail(customerId, customerFormData);
    }

    public Response deleteAddress(String customerId, String addressId) {
        return customerService.deleteAddress(customerId, addressId);
    }

    public Response setDefaultAddress(String customerId, String addressId) {
        return customerService.setDefaultAddress(customerId, addressId);
    }

    public Response placeOrder(Object checkoutObject) {
        return checkoutService.placeOrder(checkoutObject);
    }

    public Response getOrderDetails(String orderId) {
        return orderService.getOrderDetails(orderId);
    }

    public Response getAllOrdersByCustomerId(String customerId) {
        return orderService.getAllOrdersByCustomerId(customerId);
    }

    public Response getWishlist(String customerId) {
        return wishlistService.getWishlist(customerId);
    }

    public Response addToWishlist(String customerId, String productId) {
        return wishlistService.addToWishlist(customerId, productId);
    }

    public Response removeWishlistItem(String customerId, String productId) {
        return wishlistService.removeWishlistItem(customerId, productId);
    }

    public Response moveToCart(String customerId, String productId) {
        return wishlistService.moveToCart(customerId, productId);
    }
}
