package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Customer;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CustomerService {

    private ApiClient apiClient;
    private ResponseUtil responseUtil;

    @Autowired
    public CustomerService(ApiClient apiClient, ResponseUtil responseUtil) {
        this.apiClient = apiClient;
        this.responseUtil = responseUtil;
    }

    public Customer getCustomer(Customer loginUser) {
        if(Util.isNullOrEmpty(loginUser) || Util.isNullOrEmpty(loginUser.getSocialId()) || Util.isNullOrEmpty(loginUser.getEmail())) {
            return null;
        } else {
            Customer customer = getCustomerByParam(
                    new Param<>(ApiMapping.PARAM_EMAIL, loginUser.getEmail())
            );

            if(Util.isNullOrEmpty(customer) || Util.isNullOrEmpty(customer.getId())) {
                customer = createCustomer(loginUser);
            }

            return customer;
        }
    }

    public Customer getCustomerByParam(Param... params) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(params);

        Response response = apiClient.getJSON(ApiMapping.GET_CUSTOMER_BY_PARAM, null, queryString);
        return responseUtil.getDataFromResponse(response, Customer.class);
    }

    public Customer createCustomer(Customer customer) {
        Response response = apiClient.postJSON(ApiMapping.CREATE_CUSTOMER, customer);
        return responseUtil.getDataFromResponse(response, Customer.class);
    }

    public String getAccessToken(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        Response response = apiClient.getJSON(ApiMapping.GET_ACCESS_TOKEN_BY_CUSTOMER_ID, null, queryString);
        return responseUtil.getDataFromResponse(response, String.class);
    }

    public Map<String, String> getVisitorId() {
        Response response = apiClient.getJSON(ApiMapping.GET_VISITOR_ID, null, null);
        return responseUtil.getDataFromResponse(response, HashMap.class);
    }

    public boolean logout(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        Response response = apiClient.getJSON(ApiMapping.LOGOUT, null, queryString);
        return responseUtil.getDataFromResponse(response, Boolean.class);
    }

    public Response getCustomerDetail(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        return apiClient.getJSON(ApiMapping.GET_CUSTOMER_DETAIL, null, queryString);
    }

    public Response getAddressList(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        return apiClient.getJSON(ApiMapping.GET_ADDRESS_LIST, null, queryString);
    }

    public Response saveAddress(String customerId, Map<String, Object> payload) {
        payload.put(ApiMapping.PARAM_CUSTOMER_ID, customerId);
        return apiClient.postJSON(ApiMapping.SAVE_ADDRESS, payload);
    }

    public Response updateCustomerDetail(String customerId, Map<String, Object> payload) {
        payload.put(ApiMapping.PARAM_CUSTOMER_ID, customerId);
        return apiClient.postJSON(ApiMapping.UPDATE_CUSTOMER_DETAIL, payload);
    }

    public Response deleteAddress(String customerId, String addressId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_ADDRESS_ID, addressId)
        );

        return apiClient.getJSON(ApiMapping.DELETE_ADDRESS, null, queryString);
    }

    public Response setDefaultAddress(String customerId, String addressId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_ADDRESS_ID, addressId)
        );

        return apiClient.getJSON(ApiMapping.SET_DEFAULT_ADDRESS, null, queryString);
    }
}
