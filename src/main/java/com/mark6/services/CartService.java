package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.Util;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CartService {

    private ApiClient apiClient;

    @Autowired
    public CartService(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public Response addToCart(String customerId, String productId, int quantity) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId),
                new Param<>(ApiMapping.PARAM_QUANTITY, quantity)
        );

        return apiClient.getJSON(ApiMapping.ADD_TO_CART, null, queryString);
    }

    public Response getCart(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        return apiClient.getJSON(ApiMapping.GET_CART, null, queryString);
    }

    public Response removeCartItem(String cartRefId, String customerId, String cartItemId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_CART_ITEM_ID, cartItemId),
                new Param<>(ApiMapping.PARAM_CART_REF_ID, cartRefId)
        );

        return apiClient.getJSON(ApiMapping.REMOVE_CART_ITEM, null, queryString);
    }

    public Response updateQuantity(String cartRefId, String customerId, String cartItemId, String quantity) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_CART_ITEM_ID, cartItemId),
                new Param<>(ApiMapping.PARAM_QUANTITY, quantity),
                new Param<>(ApiMapping.PARAM_CART_REF_ID, cartRefId)
        );

        return apiClient.getJSON(ApiMapping.UPDATE_QUANTITY, null, queryString);
    }
}
