package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.Util;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class WishlistService {

    private ApiClient apiClient;

    @Autowired
    public WishlistService(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public Response getWishlist(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        return apiClient.getJSON(ApiMapping.GET_WISHLIST, null, queryString);
    }

    public Response addToWishlist(String customerId, String productId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId)
        );

        return apiClient.getJSON(ApiMapping.ADD_TO_WISHLIST, null, queryString);
    }

    public Response removeWishlistItem(String customerId, String productId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId)
        );

        return apiClient.getJSON(ApiMapping.REMOVE_WISHLIST_ITEM, null, queryString);
    }

    public Response moveToCart(String customerId, String productId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId),
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId)
        );

        return apiClient.getJSON(ApiMapping.MOVE_TO_CART, null, queryString);
    }
}
