package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OrderService {

    private ApiClient apiClient;
    private ResponseUtil responseUtil;

    @Autowired
    public OrderService(ApiClient apiClient, ResponseUtil responseUtil) {
        this.apiClient = apiClient;
        this.responseUtil = responseUtil;
    }

    public Response getOrderDetails(String orderId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_ORDER_ID, orderId)
        );

        return apiClient.getJSON(ApiMapping.GET_ORDER_DETAILS, null, queryString);
    }

    public Response getAllOrdersByCustomerId(String customerId) {
        Map<String, Object> queryString = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_CUSTOMER_ID, customerId)
        );

        return apiClient.getJSON(ApiMapping.GET_ORDERS_BY_CUSTOMER_ID, null, queryString);
    }
}
