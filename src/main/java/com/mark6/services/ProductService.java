package com.mark6.services;

import com.mark6.api.ApiClient;
import com.mark6.constants.ApiMapping;
import com.mark6.helper.Util;
import com.mark6.model.Param;
import com.mark6.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ProductService {

    private ApiClient apiClient;

    @Autowired
    public ProductService(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public Response getProductById(String productId) {
        Map<String, String> pathParam = Util.buildUnmodifiableParameters(
                new Param<>(ApiMapping.PARAM_PRODUCT_ID, productId)
        );

        return apiClient.getJSON(ApiMapping.GET_PRODUCT_BY_ID, pathParam, null);
    }
}
