package com.mark6.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by mhussaa on 9/15/17.
 */
@Data
public class Customer implements Serializable {
    private String id;
    private String socialId;
    private String name;
    private String email;
    private String pictureUrl;
    private String accessToken;
}
