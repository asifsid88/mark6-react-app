package com.mark6.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by mhussaa on 9/15/17.
 */
@Data
public class AddCartItem implements Serializable {
    private String pid;
    private String qty;
}
