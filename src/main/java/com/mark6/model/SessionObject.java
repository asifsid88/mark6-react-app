package com.mark6.model;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by mhussaa on 9/16/17.
 */
@Data
@Component
@Scope(proxyMode= ScopedProxyMode.TARGET_CLASS, value = "session")
public class SessionObject implements Serializable {
    private String customerId;
    private String socialId;
    private String name;
    private String email;
    private String accessToken;
}
