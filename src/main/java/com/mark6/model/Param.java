package com.mark6.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Param<K, V> implements Serializable {
    private K key;
    private V value;
}
