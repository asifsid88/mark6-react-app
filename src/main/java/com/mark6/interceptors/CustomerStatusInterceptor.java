package com.mark6.interceptors;

import com.mark6.helper.Util;
import com.mark6.services.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Log4j2
public class CustomerStatusInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private CustomerService customerService;

    /**
     * Whenever customer visits website then check for m6NSid, if it is not available then
     * associate m6VSid cookie to this customer with fresh m6id.
     * Presence of m6NSid ensures customer is logged in.
     * Presence of m6NSid ensures customer is a visitor (not logged in).
     * m6id is accessToken exchanged for this customer.
     *
     * If m6NSid is unavailable then create m6VSid.
     * If m6VSid and m6NSid both are available then delete m6VSid.
     *
     * This method will ensure at least m6VSid is always present for a customer (along with m6id).
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            // Customer is visiting website for the first time Or customer has messed up with cookies
            Map<String, String> tokens = customerService.getVisitorId();
            Util.setVisitorCookies(request, response, tokens);
        }

        return true;
    }
}
