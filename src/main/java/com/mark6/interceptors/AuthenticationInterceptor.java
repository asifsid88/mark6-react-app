package com.mark6.interceptors;

import com.mark6.api.ApiClient;
import com.mark6.helper.CookieName;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.services.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Log4j2
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private ApiClient apiClient;

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private CustomerService customerService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean isValid = isValidRequest(request);
        if(!isValid) {
            String customerId = Util.getCurrentCustomerIdFromCookie(request);
            Util.clearCookies(request, response);
            customerService.logout(customerId);
            response.sendRedirect("/login");

            return false;
        }

        return true;
    }

    private boolean isValidRequest(HttpServletRequest request) {
        String currentAccessToken = Util.getCookie(request.getCookies(), CookieName.ACCESS_TOKEN);
        String customerId = Util.getCurrentCustomerIdFromCookie(request);

        if(Util.isNullOrEmpty(currentAccessToken) || Util.isNullOrEmpty(currentAccessToken)) {
            return false;
        }

        String customerAccessToken = customerService.getAccessToken(customerId);
        return !Util.isNullOrEmpty(customerAccessToken) && currentAccessToken.equals(customerAccessToken);
    }
}
