package com.mark6.helper;

import com.mark6.model.Customer;
import com.mark6.model.Param;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by mhussaa on 9/10/17.
 */
public class Util {

    public static String getCurrentTime() {
        final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        return dateFormat.format(calendar.getTime());
    }

    @SafeVarargs
    public static <K,V> Map<K, V> buildUnmodifiableParameters(Param<? extends K, ? extends V>... param) {
        return Collections.unmodifiableMap(
                    Stream.of(param).collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));
    }

    public static boolean isNullOrEmpty(Object obj) {
        if(obj == null) return true;

        if(obj instanceof String) {
            String val = String.valueOf(obj);
            return val.trim().isEmpty();
        } else if(obj instanceof Long) {
            Long val = (Long) obj;
            return val == 0l;
        } else if(obj instanceof Integer) {
            Integer val = (Integer) obj;
            return val == 0;
        } else if(obj instanceof Float) {
            Float val = (Float) obj;
            return val == 0.0f;
        } else if(obj instanceof Double) {
            Double val = (Double) obj;
            return val == 0.0d;
        }

        return false;
    }

    public static String encode(String str) {
        return Base64.getEncoder()
                     .withoutPadding()
                     .encodeToString(str.getBytes(StandardCharsets.UTF_8));
    }

    public static String decode(String str) {
        return new String(Base64.getDecoder()
                                .decode(str), StandardCharsets.UTF_8);
    }

    public static String getCookie(Cookie[] cookies, String cookieName) {
        if(isNullOrEmpty(cookies)) return null;

        return Arrays.stream(cookies)
                .filter(c -> c.getName().equals(cookieName))
                .findFirst()
                .map(Cookie::getValue)
                .orElse(null);
    }

    public static String getCookie(HttpServletRequest request, String cookieName) {
        return getCookie(request.getCookies(), cookieName);
    }

    public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for(Cookie cookie : cookies) {
                if(cookie.getName().equals(cookieName)) {
                    cookie.setValue("");
                    cookie.setPath("/");
                    cookie.setMaxAge(0);

                    response.addCookie(cookie);
                    break;
                }
            }
        }
    }

    public static void clearCookies(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);

                response.addCookie(cookie);
            }
        }
    }

    /**
     *
     * @param response HttpResponse where cookie will be added
     * @param name Name of Cookie
     * @param value Value of Cookie
     * @param expiry Expiration in number of days. If positive number then that many days cookie is set
     *               If negative number, then cookie will not be persisted, will be deleted with browser closing
     *               If zero, then cookie is unset
     */
    public static void setCookie(HttpServletResponse response, String name, String value, int expiry) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(expiry * 24 * 60 * 60);
        cookie.setPath("/");

        response.addCookie(cookie);
    }

    public static void setLoginCookies(HttpServletRequest request, HttpServletResponse response, Customer customer) {
        /**
         * m6NSid is encrypted customerId
         */
        setCookie(response, CookieName.CUSTOMER_ID, encode(customer.getId()), 10);

        /**
         * m6id is accessToken
         */
        setCookie(response, CookieName.ACCESS_TOKEN, customer.getAccessToken(), 10);

        /**
         * Customer is logged in so delete m6VSid
         */
        removeCookie(request, response, CookieName.VISITOR_ID);
    }

    public static void setVisitorCookies(HttpServletRequest request, HttpServletResponse response, Map<String, String> tokens) {
        /**
         * m6VSid is encrypted visitorId
         */
        Util.setCookie(response, CookieName.VISITOR_ID, encode(tokens.get("visitorId")), 1);

        /**
         * m6id is accessToken
         */
        setCookie(response, CookieName.ACCESS_TOKEN, tokens.get("accessToken"), 10);
    }

    /**
     * Get current customer id from cookie.
     * @param request HttpServletRequest from which customerId will be fetched.
     * @return decoded CustomerId.
     */
    public static String getCurrentCustomerIdFromCookie(HttpServletRequest request) {
        String customerId = getCookie(request, CookieName.CUSTOMER_ID);
        if(Util.isNullOrEmpty(customerId)) {
            // Customer is not logged in: Check for m6VSid.
            String visitorId = Util.getCookie(request, CookieName.VISITOR_ID);
            if(!Util.isNullOrEmpty(visitorId)) {
                customerId = visitorId;
            } else {
                return null;
            }
        }

        return decode(customerId);
    }

    public static boolean isUserLoggedIn(HttpServletRequest request) {
        String customerId = getCookie(request, CookieName.CUSTOMER_ID);
        return !isNullOrEmpty(customerId);
    }
}
