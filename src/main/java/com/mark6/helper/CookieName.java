package com.mark6.helper;

/**
 * Created by mhussaa on 9/10/17.
 */
public interface CookieName {
    String ACCESS_TOKEN = "m6id";
    String CUSTOMER_ID = "m6NSid";
    String VISITOR_ID = "m6VSid";
}
