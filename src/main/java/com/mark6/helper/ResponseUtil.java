package com.mark6.helper;

import com.google.gson.Gson;
import com.mark6.constants.ResponseStatus;
import com.mark6.model.Response;
import org.springframework.stereotype.Component;

/**
 * Created by mhussaa on 9/10/17.
 */
@Component
public class ResponseUtil {

    public Response buildResponse(Object data) {
        Response response;
        if(data == null) {
            response = buildResponse(ResponseStatus.FAIL, new Object());
        } else {
            response = buildResponse(ResponseStatus.OK, data);
        }

        return response;
    }

    public Response buildResponse(ResponseStatus responseStatus, Object data) {
        Response response = new Response();
        response.setStatus(responseStatus.getCode());
        response.setMessage(responseStatus.getDescription());
        response.setData(data);

        return response;
    }

    public <T> T getDataFromResponse(Response response, Class<T> classOfT) {
        if(!Util.isNullOrEmpty(response) && response.getStatus() == 200 && !Util.isNullOrEmpty(response.getData())) {
            String json = new Gson().toJson(response.getData());
            return new Gson().fromJson(json, classOfT);
        }

        return null;
    }
}
