package com.mark6.constants;

/**
 * Created by mhussaa on 9/2/17.
 */
public interface URLMapping {
    /* Start: IndexController Mapping *****/
    String INDEX = "/";
    String ABOUT_US = "/about-us";
    String CATALOG = "/catalog";
    String CONTACT_US = "/contact-us";
    String LOGIN = "/login";
    String GET_PRODUCT = "/{productName}/p/{productId}";
    String CART = "/cart";
    String CHECKOUT = "/checkout/init";
    String ORDER_DETAILS = "/order_details";

    /* Account ****/
    String ACCOUNT_HOME = "/account/home";
    String ACCOUNT_ORDERS = "/account/orders";
    String ACCOUNT_WISHLIST = "/account/wishlist";
    String ACCOUNT_SUBSCRIPTIONS = "/account/subscriptions";
    String ACCOUNT_ADDRESSES = "/account/addresses";
    String ACCOUNT_LOGOUT = "/account/logout";
    /* End: IndexController Mapping *****/

    /* Start: URL Mapping for APIs ****/
    // GenericController
    String API_ROOT = "api/";
    String API_LOGIN = "login";

    // AccountController
    String API_ACCOUNT_ROOT = "api/account";
    String API_LOGOUT = "logout";

    // OrderController
    String API_ORDER_ROOT = "api/order";
    String API_GET_ORDER_DETAILS = "details";
    String API_GET_ORDERS_BY_CUSTOMER_ID = "all";

    // CartController
    String API_CART_ROOT = "api/cart";
    String API_ADD_TO_CART = "add";
    String API_GET_CART = "home";
    String API_REMOVE_CART_ITEM = "remove";
    String API_UPDATE_QUANTITY = "updateqty";

    // CheckoutController
    String API_CHECKOUT_ROOT = "api/checkout";
    String API_CHECKOUT_INIT = "init";
    String API_SELECT_ADDRESS = "selectAddress";
    String API_PLACE_ORDER = "placeOrder";

    // CatalogController
    String API_CATALOG_ROOT = "api/catalog";
    String API_GET_CATALOG = "/home";

    // CustomerController
    String API_CUSTOMER_ROOT = "api/customer";
    String API_IS_USER_LOGGED_IN = "isLogin";
    String API_GET_CUSTOMER_DETAIL = "detail";
    String API_GET_ADDRESS_LIST = "addresses";
    String API_SAVE_ADDRESS = "saveAddress";
    String API_DELETE_ADDRESS = "deleteAddress";
    String API_SET_DEFAULT_ADDRESS = "setDefaultAddress";
    String API_UPDATE_CUSTOMER_DETAIL = "updateDetail";

    // WishlistController
    String API_WISHLIST_ROOT = "api/wishlist";
    String API_ADD_TO_WISHLIST = "add";
    String API_GET_WISHLIST = "home";
    String API_REMOVE_WISHLIST_ITEM = "remove";
    String API_MOVE_TO_CART = "move";

    // ProductController
    String API_PRODUCT_ROOT = "api/product";
    String API_GET_PRODUCT_BY_ID = "{productId}";
    /* End: URL Mapping for APIs ****/

}
