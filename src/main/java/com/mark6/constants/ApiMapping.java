package com.mark6.constants;

/**
 * Created by mhussaa on 9/11/17.
 */
public interface ApiMapping {
    /* ProductService ***/
    String GET_PRODUCT_BY_ID = "product/{productId}";

    /* CatalogService ***/
    String GET_CATALOG = "catalog/";

    /* CustomerService ***/
    String GET_CUSTOMER_BY_ID = "customer/{customerId}";
    String GET_CUSTOMER_BY_PARAM = "customer/";
    String CREATE_CUSTOMER = "customer/create";
    String GET_ACCESS_TOKEN_BY_CUSTOMER_ID = "customer/accesstoken";
    String LOGOUT = "customer/logout";
    String GET_VISITOR_ID = "customer/visitorid";
    String GET_CUSTOMER_DETAIL = "customer/detail";
    String GET_ADDRESS_LIST = "customer/addresses";
    String SAVE_ADDRESS = "customer/saveAddress";
    String DELETE_ADDRESS = "customer/deleteAddress";
    String SET_DEFAULT_ADDRESS = "customer/setDefaultAddress";
    String UPDATE_CUSTOMER_DETAIL = "customer/updateDetail";

    /* CartService ***/
    String ADD_TO_CART = "cart/add";
    String GET_CART = "cart/home";
    String REMOVE_CART_ITEM = "cart/remove";
    String UPDATE_QUANTITY = "cart/updateqty";

    /* CheckoutService ***/
    String CHECKOUT_INIT = "checkout/init";
    String SELECT_ADDRESS = "checkout/selectAddress";
    String PLACE_ORDER = "checkout/placeOrder";

    /* OrderService ***/
    String GET_ORDER_DETAILS = "order/details";
    String GET_ORDERS_BY_CUSTOMER_ID = "order/all";

    /* WishlistService ***/
    String GET_WISHLIST = "wishlist/home";
    String ADD_TO_WISHLIST = "wishlist/add";
    String REMOVE_WISHLIST_ITEM = "wishlist/remove";
    String MOVE_TO_CART = "wishlist/move";

    /* Path Params and Query Strings ****/
    String PARAM_PRODUCT_ID = "productId";
    String PARAM_CUSTOMER_ID = "customerId";
    String PARAM_CATALOG_ID = "catalogId";
    String PARAM_EMAIL = "email";
    String PARAM_QUANTITY = "quantity";
    String PARAM_CART_ITEM_ID = "cartItemId";
    String PARAM_CART_REF_ID = "cartRefId";
    String PARAM_ADDRESS_ID = "addressId";
    String PARAM_ORDER_ID = "orderId";
}
