package com.mark6.controllers.api;

import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(URLMapping.API_ACCOUNT_ROOT)
public class AccountController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public AccountController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_LOGOUT, method = RequestMethod.GET)
    public @ResponseBody
    Response logout(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        Util.clearCookies(request, response);

        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return responseUtil.buildResponse(service.logout(customerId));
    }

}
