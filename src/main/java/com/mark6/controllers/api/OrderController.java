package com.mark6.controllers.api;

import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(URLMapping.API_ORDER_ROOT)
public class OrderController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public OrderController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_GET_ORDER_DETAILS, method = RequestMethod.GET)
    public @ResponseBody Response getOrderDetails(@RequestParam(value = ApiMapping.PARAM_ORDER_ID) String orderId) {
        return service.getOrderDetails(orderId);
    }

    @RequestMapping(value = URLMapping.API_GET_ORDERS_BY_CUSTOMER_ID, method = RequestMethod.GET)
    public @ResponseBody Response getOrdersByCustomerID(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.getAllOrdersByCustomerId(customerId);
    }
}
