package com.mark6.controllers.api;

import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.AddCartItem;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(URLMapping.API_CART_ROOT)
public class CartController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public CartController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_ADD_TO_CART, method = RequestMethod.POST)
    public @ResponseBody
    Response addToCart(HttpServletRequest request, HttpServletResponse response,
                       @RequestBody(required = false) AddCartItem cartItem) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        if(Util.isNullOrEmpty(cartItem.getPid())) {
            // TODO: Say unable to add cartItem as pid is null
            return responseUtil.buildResponse(null);
        }

        int quantity = 1;
        if(!Util.isNullOrEmpty(cartItem.getQty())) {
            quantity = Integer.parseInt(cartItem.getQty());
        }

        return service.addToCart(customerId, cartItem.getPid(), quantity);
    }

    @RequestMapping(value = URLMapping.API_GET_CART, method = RequestMethod.GET)
    public @ResponseBody Response getCart(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.getCart(customerId);
    }

    @RequestMapping(value = URLMapping.API_REMOVE_CART_ITEM, method = RequestMethod.GET)
    public @ResponseBody Response removeCartItem(HttpServletRequest request, HttpServletResponse response,
                                                 @RequestParam(ApiMapping.PARAM_CART_ITEM_ID) String cartItemId,
                                                 @RequestParam(ApiMapping.PARAM_CART_REF_ID) String cartRefId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.removeCartItem(cartRefId, customerId, cartItemId);
    }

    @RequestMapping(value = URLMapping.API_UPDATE_QUANTITY, method = RequestMethod.GET)
    public @ResponseBody Response updateQuantity(HttpServletRequest request, HttpServletResponse response,
                                                 @RequestParam(ApiMapping.PARAM_CART_ITEM_ID) String cartItemId,
                                                 @RequestParam(ApiMapping.PARAM_QUANTITY) String quantity,
                                                 @RequestParam(ApiMapping.PARAM_CART_REF_ID) String cartRefId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.updateQuantity(cartRefId, customerId, cartItemId, quantity);
    }
}
