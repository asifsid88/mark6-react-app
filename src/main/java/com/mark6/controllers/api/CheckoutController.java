package com.mark6.controllers.api;


import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(URLMapping.API_CHECKOUT_ROOT)
public class CheckoutController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public CheckoutController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_CHECKOUT_INIT, method = RequestMethod.GET)
    public @ResponseBody Response init(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = ApiMapping.PARAM_PRODUCT_ID) String productId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.checkoutInit(customerId, productId);
    }

    @RequestMapping(value = URLMapping.API_SELECT_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response selectAddress(HttpServletRequest request, HttpServletResponse response,
                                                @RequestParam(ApiMapping.PARAM_ADDRESS_ID) String addressId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.selectAddress(customerId, addressId);
    }

    @RequestMapping(value = URLMapping.API_PLACE_ORDER, method = RequestMethod.POST)
    public @ResponseBody Response placeOrder(HttpServletRequest request, HttpServletResponse response,
                                             @RequestBody Object checkoutObject) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.placeOrder(checkoutObject);
    }
}
