package com.mark6.controllers.api;

import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(URLMapping.API_WISHLIST_ROOT)
public class WishlistController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public WishlistController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_GET_WISHLIST, method = RequestMethod.GET)
    public @ResponseBody Response getWishlist(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.getWishlist(customerId);
    }

    @RequestMapping(value = URLMapping.API_ADD_TO_WISHLIST, method = RequestMethod.GET)
    public @ResponseBody Response addToWishlist(HttpServletRequest request, HttpServletResponse response,
                                                @RequestParam(value = ApiMapping.PARAM_PRODUCT_ID) String productId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.addToWishlist(customerId, productId);
    }

    @RequestMapping(value = URLMapping.API_REMOVE_WISHLIST_ITEM, method = RequestMethod.GET)
    public @ResponseBody Response removeItem(HttpServletRequest request, HttpServletResponse response,
                                             @RequestParam(value = ApiMapping.PARAM_PRODUCT_ID) String productId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.removeWishlistItem(customerId, productId);
    }

    @RequestMapping(value = URLMapping.API_MOVE_TO_CART, method = RequestMethod.GET)
    public @ResponseBody Response moveToCart(HttpServletRequest request, HttpServletResponse response,
                                             @RequestParam(value = ApiMapping.PARAM_PRODUCT_ID) String productId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.moveToCart(customerId, productId);
    }
}
