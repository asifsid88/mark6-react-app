package com.mark6.controllers.api;

import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(URLMapping.API_CATALOG_ROOT)
public class CatalogContoller {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public CatalogContoller(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_GET_CATALOG, method = RequestMethod.GET)
    public @ResponseBody
    Response getCatalog(@RequestParam(value = ApiMapping.PARAM_CATALOG_ID) String catalogId) {
        return service.getCatalog(catalogId);
    }
}
