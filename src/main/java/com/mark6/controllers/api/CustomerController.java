package com.mark6.controllers.api;

import com.mark6.constants.ApiMapping;
import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping(URLMapping.API_CUSTOMER_ROOT)
public class CustomerController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public CustomerController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_IS_USER_LOGGED_IN, method = RequestMethod.GET)
    public @ResponseBody Response isLogin(HttpServletRequest request, HttpServletResponse response) {
        boolean isUserLoggedIn = Util.isUserLoggedIn(request);
        return responseUtil.buildResponse(isUserLoggedIn);
    }

    @RequestMapping(value = URLMapping.API_GET_CUSTOMER_DETAIL, method = RequestMethod.GET)
    public @ResponseBody Response getCustomerDetail(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.getCustomerDetail(customerId);
    }

    @RequestMapping(value = URLMapping.API_GET_ADDRESS_LIST, method = RequestMethod.GET)
    public @ResponseBody Response getAddressList(HttpServletRequest request, HttpServletResponse response) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.getAddressList(customerId);
    }

    @RequestMapping(value = URLMapping.API_SAVE_ADDRESS, method = RequestMethod.POST)
    public @ResponseBody Response saveAddress(HttpServletRequest request, HttpServletResponse response,
                                              @RequestBody Map<String, Object> saveAddressFormData) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.saveAddress(customerId, saveAddressFormData);
    }

    @RequestMapping(value = URLMapping.API_UPDATE_CUSTOMER_DETAIL, method = RequestMethod.POST)
    public @ResponseBody Response updateCustomerDetail(HttpServletRequest request, HttpServletResponse response,
                                              @RequestBody Map<String, Object> customerFormData) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.updateCustomerDetail(customerId, customerFormData);
    }

    @RequestMapping(value = URLMapping.API_DELETE_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response deleteAddress(HttpServletRequest request, HttpServletResponse response,
                                                @RequestParam(value = ApiMapping.PARAM_ADDRESS_ID) String addressId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.deleteAddress(customerId, addressId);
    }

    @RequestMapping(value = URLMapping.API_SET_DEFAULT_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response setDefaultAddress(HttpServletRequest request, HttpServletResponse response,
                                                @RequestParam(value = ApiMapping.PARAM_ADDRESS_ID) String addressId) {
        String customerId = Util.getCurrentCustomerIdFromCookie(request);
        if(Util.isNullOrEmpty(customerId)) {
            return responseUtil.buildResponse(null);
        }

        return service.setDefaultAddress(customerId, addressId);
    }
}
