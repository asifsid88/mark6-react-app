package com.mark6.controllers;

import com.mark6.constants.URLMapping;
import com.mark6.constants.ViewName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mhussaa on 3/14/17.
 */
@Controller
@RequestMapping(URLMapping.INDEX)
public class Mark6IndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ABOUT_US, method = RequestMethod.GET)
    public String aboutus() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.CONTACT_US, method = RequestMethod.GET)
    public String contactus() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.CATALOG, method = RequestMethod.GET)
    public String catalog() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.LOGIN, method = RequestMethod.GET)
    public String login() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.GET_PRODUCT, method = RequestMethod.GET)
    public String product() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_HOME, method = RequestMethod.GET)
    public String accountHomoe() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_ORDERS, method = RequestMethod.GET)
    public String accountOrders() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_WISHLIST, method = RequestMethod.GET)
    public String accountWishlist() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_SUBSCRIPTIONS, method = RequestMethod.GET)
    public String accountSubscriptions() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_ADDRESSES, method = RequestMethod.GET)
    public String accountAddresses() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ACCOUNT_LOGOUT, method = RequestMethod.GET)
    public String accountLogout() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.CART, method = RequestMethod.GET)
    public String getCart() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.CHECKOUT, method = RequestMethod.GET)
    public String checkout() {
        return ViewName.INDEX;
    }

    @RequestMapping(value = URLMapping.ORDER_DETAILS, method = RequestMethod.GET)
    public String orderDetails() {
        return ViewName.INDEX;
    }
}
