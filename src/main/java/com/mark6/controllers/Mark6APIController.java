package com.mark6.controllers;

import com.mark6.constants.URLMapping;
import com.mark6.helper.ResponseUtil;
import com.mark6.helper.Util;
import com.mark6.model.Customer;
import com.mark6.model.Response;
import com.mark6.model.SessionObject;
import com.mark6.services.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.API_ROOT)
public class Mark6APIController {

    private ServiceFacade service;
    private SessionObject sessionObject;
    private ResponseUtil responseUtil;

    @Autowired
    public Mark6APIController(ServiceFacade service, SessionObject sessionObject, ResponseUtil responseUtil) {
        this.service = service;
        this.sessionObject = sessionObject;
        this.responseUtil = responseUtil;
    }

    @RequestMapping(value = URLMapping.API_LOGIN, method = RequestMethod.POST)
    public @ResponseBody Response login(HttpServletRequest request, HttpServletResponse response, @RequestBody(required = false) Customer loginUser) {
        Customer customer = service.getCustomer(loginUser);
        if(!Util.isNullOrEmpty(customer)) {
            String accessToken = service.getAccessToken(customer.getId());
            if(!Util.isNullOrEmpty(accessToken)) {
                customer.setAccessToken(accessToken);

                sessionObject.setCustomerId(customer.getId());
                sessionObject.setEmail(customer.getEmail());
                sessionObject.setName(customer.getName());
                sessionObject.setSocialId(customer.getSocialId());
                sessionObject.setAccessToken(accessToken);

                Util.setLoginCookies(request, response, customer);
            } else {
                customer = null;
            }
        }

        return responseUtil.buildResponse(customer);
    }
}
