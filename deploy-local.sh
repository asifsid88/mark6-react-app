#!/bin/bash
echo 'Deleting target folder'
rm -rf target

echo 'Webpack: Building and packaging Mark6 ReactApp'
webpack --env.NODE_ENV=dev

echo 'Building and packaging Mark6 Application WebApp'
mvn clean compile install package


echo 'Stopping tomcat'
touch temp.txt
lsof -n -i4TCP:8080 > temp.txt
tomcatProcess=`sed -n '2p' temp.txt`
tomcatProcessId=$(echo $tomcatProcess | awk '{print $2}')
if [[ -n $tomcatProcessId ]]
then
    echo 'Tomcat running with PID='$tomcatProcessId
    kill -9 $tomcatProcessId
    echo 'Tomcat running at 8080 is stopped.'
else
    echo 'Your tomcat is already stopped.'
fi


echo 'Run Mark6 webapp'
java -jar -Denv=dev target/dependency/webapp-runner.jar target/*.war

echo 'Mark6 Webapp running on: http://localhost:8080/'